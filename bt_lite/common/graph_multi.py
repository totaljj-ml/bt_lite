import random
import string

import plotly.graph_objects as go
import yaml
from plotly.subplots import make_subplots

from bt_lite.common import const


def draw(df_cds, df_trades):

    symbols = df_cds['symbol'].unique()
    fig = make_subplots(rows=len(symbols), cols=1,
                        specs=[
                            [{}],
                            [{}],
                        ],
                        shared_xaxes=True,
                        subplot_titles=("First Subplot", "Second Subplot"))

    for i, symbol in enumerate(symbols):

        df_cds_sym = df_cds[df_cds['symbol'] == symbol]
        df_trades_sym = df_trades[df_trades['symbol'] == symbol]

        fig1 = go.Candlestick(x=df_cds_sym['time'], open=df_cds_sym['open'], high=df_cds_sym['high'],
                              low=df_cds_sym['low'], close=df_cds_sym['close'])


        anns = []
        for i, df_trade_sym in df_trades_sym.iterrows():
            time_ = df_trade_sym['time']
            side = df_trade_sym['side']
            price = df_trade_sym['price']
            if (side == 1):
                anns[i].add_annotation(x=time_, y=price, text="long", showarrow=True, yshift=0, font=dict( family="Courier New, monospace", size=16, color="#2ca02c" ), arrowhead=2, arrowsize=1, arrowwidth=2, arrowcolor="#2ca02c", )
                # anns.append({x=time_, y=price, text="long", showarrow=True, yshift=0, font=dict( family="Courier New, monospace", size=16, color="#2ca02c" ), arrowhead=2, arrowsize=1, arrowwidth=2, arrowcolor="#2ca02c"})
            elif (side == -1):
                anns[i].add_annotation(x=time_, y=price, text="short", showarrow=True, yshift=0, font=dict( family="Courier New, monospace", size=16, color="#d62728" ), arrowhead=2, arrowsize=1, arrowwidth=2, arrowcolor="#d62728", )
                # anns.append({x=time_, y=price, text="long", showarrow=True, yshift=0, font=dict( family="Courier New, monospace", size=16, color="#2ca02c" ), arrowhead=2, arrowsize=1, arrowwidth=2, arrowcolor="#2ca02c"})

        # for i, df_trade_sym in df_trades_sym.iterrows():
        fig['layout'][i].update(annotations=anns[i])

        #todo
        #todo
        fig.add_trace(fig1, row=i + 1, col=1)
        fig.update_layout(showlegend=False, title_text="Specs with Subplot Title", xaxis_rangeslider_visible=False)

    fig.show()





