import datetime

import numpy as np
import pandas as pd
pd.set_option('mode.chained_assignment',  None)

from bt_lite.common import const, util, graph, graph_multi
from bt_lite.common.util import pct, divide_chunks
from bt_lite.core import Order, Broker, Account, Optimizer
from bt_lite.indicators import moving_average, tops
from bt_lite.strategy import Strategy
from test.test_util import read



#일반 vola랑 값 확인

#leverage by last trade, mdd?
#이격도

class VolaStrTwo(Strategy):

    def __init__(self):
        super().__init__()
        self.config = None

        self.tgt_price = None


    def get_indicators(self, data, indicators):
        ##1d##
        #shift
        data['high_s1'] = data.groupby('symbol')['high'].shift()
        data['low_s1'] = data.groupby('symbol')['low'].shift()
        data['close_s1'] = data.groupby('symbol')['close'].shift()

        return data

    def execute(self, state):
        #ORDERS
        orders = []

        #ACCOUNT
        account = state['account']

        #DATA
        data = state['data']
        time = data['time']
        symbol = data['symbol']

        #TEST
        # if(symbol != 'ETH/USDT'): return orders

        is_open_bar = data['is_open_bar']
        if(is_open_bar):
            data['price'] = data['open']
        else:
            data['price'] = data['close']

        #BALANCE
        margin_balance = account.margin_balance
        #todo check logic
        margin_balance_low = account.margin_balance_low
        if(margin_balance_low < 0):
            return orders

        #POSITION
        position = account.positions[symbol]
        position_side = position.side
        position_qty = position.qty

        #CALCULATE TGT_PRICE
        k = self.config['k']
        vola = data['high_s1'] - data['low_s1']
        self.tgt_price = vola * k + data['open']

        #LEVERAGE
        lev = self.config['lev']

        #NUM_SPLITS
        num_splits = self.config['num_splits']

        #MAX HOLD SYMBOLS
        max_hold_symbols = self.config['max_hold_symbols']

        #todo 다른 strategy 도
        #todo
        #margin_balance_at_open
        margin_balance_at_open = position.margin_balance_at_open if(position.margin_balance_at_open) else account.margin_balance
        max_balance_position = margin_balance_at_open / max_hold_symbols

        #pct unpnl
        pct_unpnl = 0
        if(position.side != const.Position.NONE):
            pct_unpnl = pct(max_balance_position, max_balance_position + position.unrealized_pnls[-1])


        def _open_long(data, tgt_price, qty_open, orders):
            # LONG
            if (
                    (tgt_price > data['low']) &
                    (tgt_price < data['high'])
            ):
                #DEBUG
                # vol = qty_open * tgt_price
                # if(int(vol) == 1065):
                #     #todo
                #     print(vol)

                order = Order(time=time, symbol=symbol, side=const.OrderType.LONG, qty=qty_open, price=tgt_price)
                orders.append(order)

                # margin_balance_at_open
                # margin_balance_at_open
                # margin_balance_at_open
                # margin_balance_at_open
                # print('long:\n{}\n'.format(time))

        # AT OPEN PRICE (ONLY OPENING IS AVAILABLE)
        if(is_open_bar):
            qty_open = max_balance_position * lev / num_splits / max_hold_symbols / self.tgt_price

            #todo wallet vs margin
            # qty_open = margin_balance * lev / num_splits / max_hold_symbols / self.tgt_price

            if (position_side == const.Position.NONE):
                _open_long(data, self.tgt_price, qty_open, orders)

            # elif (position_side == const.Position.LONG):
            #     split buy
            #     if(pct_unpnl < -10/100):
            #         _open_long(data, self.tgt_price, qty_open, orders)

            # elif (position_side == const.Position.SHORT):
            #     split buy
            #     if(pct_unpnl < -10/100):
            #         _open_short(data, self.tgt_price, qty_open, orders)



        # AT CLOSE PRICE (ONLY CLOSING IS AVAILABLE)
        else:
           if (position_side == const.Position.LONG):
               if(True):
                   #변동성 돌파 : 청산 at close
                   #debug
                   vol = position_qty * data['close']

                   order = Order(time=time, symbol=symbol, side=const.OrderType.SHORT, qty=position_qty, price=data['close'])
                   orders.append(order)

               #tp
               # elif(pct_unpnl > 10/100):
               #     # debug
               #     vol = qty_open * data['open']
               #     order = Order(time=time, symbol=symbol, side=const.OrderType.SHORT, qty=position_qty, price=data['close'])
               #     orders.append(order)

           # if (position_side == const.Position.SHORT):
           #         order = Order(time=time, symbol=symbol, side=const.OrderType.Long, qty=position_qty, price=data['close'])
           # todo 다른 symbol이 wallet balance 미리 업데이트 치면, margin_balance로 하기?

        return orders

def run_single(df_cds, config, nlargest, fee):
    df_cds_sub = df_cds.copy()

    ##STRATEGY##
    strategy = VolaStrTwo()

    # get_indicators
    indicators = {
        'emas':[7,14,25,99]
    }
    df_cds_sub = strategy.get_indicators(df_cds_sub, indicators)
    # df_cds_sub = df_cds_sub[df_cds_sub['time'] > '2021-05-16']

    #todo pctcum 이상
    data_list = util.df2list_by_date_group(df_cds_sub)

    #config
    strategy.config = config

    ##ACCOUNT##
    INITIAL_BALANCE = 1e3
    initial_time = df_cds_sub.iloc[0]['time']
    account = Account(initial_time, INITIAL_BALANCE)

    ##RUN BROKER##
    broker = Broker(account, data_list, strategy, fee)
    broker.run()

    #final balance
    final_balance = broker.account.margin_balance
    print('final_balance:\n{}\n'.format(final_balance))

    pctcum = pct(INITIAL_BALANCE, final_balance)
    print('final_balance pctcum :\n{}\n'.format(pctcum))

    ##draw##
    df_trades = broker.get_trades()

    #debug
    cols = ['time','price','open_price','open_qty','side','realized_pnl','pct','pct_margin']
    cols = ['time','price','pct','pct_margin','margin_balance']
    print('df_trades:\n{}\n'.format(df_trades[cols]))
    df_trades.to_csv('{}/test.csv'.format(const.ROOT_DIR), index=False)

    df_trades_close = df_trades[df_trades['side'] == -1]
    df_trades_close['pct1'] = df_trades_close['pct'] + 1
    df_trades_close['pctcum'] = df_trades_close['pct1'].cumprod()
    print('df_trades_close:\n{}\n'.format(df_trades_close[cols]))

    # symbols = df_cds['symbol'].unique()
    # for symbol in symbols:
    #     df_cds_sym = df_cds[df_cds['symbol'] == symbol]
    #     df_trades_sym = df_trades[df_trades['symbol'] == symbol]
    #todo
    #todo
    graph_multi.draw(df_cds, df_trades)

    return broker


def run_opt(df_cds, fee, strategy_name, max_hold_symbols):
    ##
    df_cds_sub = df_cds.copy()
    print('len(df_cds_sub):\n{}\n'.format(len(df_cds_sub)))

    ##STRATEGY##
    strategy = VolaStrTwo()

    # get_indicators
    indicators = { 'emas': [2,3,5,7,14,21,25,99], }
    df_cds_sub = strategy.get_indicators(df_cds_sub, indicators)

    #2list
    # data_list = util.df2list(df_cds_sub)
    # data_list = list(divide_chunks(data_list, nlargest))
    data_list = util.df2list_by_date_group(df_cds_sub)

    ##OPTIMIZER##
    optimizer = Optimizer()

    #params
    params = {}

    #large range
    # params['lev'] = [2,3,4]
    params['lev'] = [4]
    params['k'] = [0.4]
    params['num_splits'] = [1]
    # params['long1_pnl_pct'] = []
    # params['long2_pnl_pct'] =
    params['pct_tp'] = np.arange(-0.02, 0, 0.01).tolist()
    params['max_hold_symbols'] = [max_hold_symbols]


    optimizer.shuffle_params(params)
    param_batch = optimizer.get_cartesian_params(params)

    #run
    results = optimizer.optimize(data_list, strategy, param_batch, fee, strategy_name)
    df = pd.DataFrame(results)
    print(df.sort_values('pctcum')[-10:][['pctcum','sr','max_drawdown','num_trades']])


if __name__ == '__main__':

    #FEE
    # LIMIT_FEE = 0.036 / 100
    LIMIT_FEE = 0
    fee = LIMIT_FEE

    #PARAMS
    exchange = 'binance_future'
    # exchange = 'binance_spot'

    frame = '1d'

    symbols = ['ETH/USDT','LUNA/USDT']
    # symbols = ['ETH/USDT']
    # symbols = ['LUNA/USDT']
    # symbols = ['BTC/USDT']


    #nlargest
    # nlargest = 1 #TEST
    nlargest = len(symbols)

    #max_hold_symbols
    max_hold_symbols = nlargest

    ##READ##
    df_cds = read(exchange, frame, symbols)

    #liquidiate
    # df_cds = df_cds[df_cds['time'] >= '2020-03-08']
    # df_cds = df_cds[df_cds['time'] >= '2020-03-01']

    #test
    # df_cds = df_cds[df_cds['time'] >= '2021-11-12']
    # df_cds = df_cds[df_cds['time'] < '2022-03-24']

    # sync vola_simple and vola1d strategy two
    df_cds = df_cds[df_cds['time'] >= '2021-07-11']
    df_cds = df_cds[df_cds['time'] < '2022-03-24']


    df_cds = df_cds.reset_index(drop=True)

    #tops by volume 1d
    ma = 1
    moving_average.set_volume_ma(df_cds, ma)
    df_cds_tops = tops.get_nlargest(df_cds, 'vol_ma{}_s1'.format(ma), nlargest)
    df_cds_tops = df_cds_tops.reset_index(drop=True)
    print('df_cds_tops range:{}~{}'.format(df_cds_tops.iloc[0]['time'], df_cds_tops.iloc[-1]['time']))

    #repeat to loop the bar two times(at open and close)
    df_cds_tops = util.split_to_open_close(df_cds_tops)

    ##RUN SINGLE##
    config = {
        'lev' : 4,
        # 'lev' : 2,
        # 'lev' : 4*2, #TEST
        'k': 0.4,
        'num_splits':1,
        'max_hold_symbols':max_hold_symbols,
        # 'pct_tp': 40 / 100
    }


    #run
    broker = run_single(df_cds_tops, config, nlargest, fee)

    margin_balances = broker.account.margin_balances
    print('margin_balances first: {}'.format(margin_balances[:5]))
    print('margin_balances last: {}'.format(margin_balances[-5:]))

    #DEBUG
    df_margin_balances = pd.DataFrame(margin_balances)
    df_margin_balances = df_margin_balances.sort_values('margin_balance_low')
    df_margin_balances.to_csv('{}/test.csv'.format(const.ROOT_DIR), index=False)
    print('df_margin_balances :\n{}'.format(df_margin_balances))

    #result
    result, _, _ = broker.get_result_stats()
    print('run_single result:\n{}\n'.format(result))

    ##RUN OPT##
    strategy_name = 'vola'
    run_opt(df_cds_tops, fee, strategy_name, max_hold_symbols)


