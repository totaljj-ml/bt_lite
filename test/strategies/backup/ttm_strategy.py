from itertools import product

import quantstats as qs

from bt_lite.core import Order, Broker, Account, Optimizer
from test.test_util import read, get_tops_by_vol

qs.extend_pandas()

import pandas as pd
import numpy as np

from bt_lite.common import const, util
from bt_lite.common.util import pct, divide_chunks
from bt_lite.indicators import ttm
from bt_lite.strategy import Strategy

class TTMStrategy(Strategy):
    def __init__(self):
        super().__init__()
        self.config = None

    #mas, atr_mas
    def get_indicators(self, data, indicators):
        df_cds = data


        #shift ohlc
        cols_shift = ['open', 'high', 'low', 'close']
        for col_shift in cols_shift:
            util.shift(df_cds, col_shift, 1)

        ##atr##

        indicators['length'] = [20,30]
        indicators['mult'] = [1,1.5,2,2.5,3]
        indicators['length_KC'] = [20,30]
        indicators['mult_KC'] = [1,1.5,2,2.5,3]

        def get_cartesian_params(params):
            params = [dict(zip(params, v)) for v in product(*params.values())]
            return params
        params = get_cartesian_params(indicators)
        for param in params:
            ttm.set_ttm_mm(df_cds, param['length'], param['mult'], param['length_KC'], param['mult_KC'])

        return df_cds

    def execute(self, state):

        #orders
        orders = []

        #1d
        # df_cds1d = self.df_cds1d

        #account
        account = state['account']

        #data
        data = state['data']
        time = data['time']
        symbol = data['symbol']

        #balance
        wallet_balance = account.wallet_balance
        margin_balance_low = account.margin_balance_low
        if(wallet_balance < 0): return orders
        if(margin_balance_low < 0):
            # print('wallet_balance_low < 0')
            return orders

        


        #position
        position = account.positions[symbol]
        position_side = position.side
        position_qty = position.qty
        pct_unpnl = position.unrealized_pnl

        #leverage
        lev = self.config['lev']

        #num_splits
        num_splits = self.config['num_splits']

        ##이격도##
        if (position_side == const.Position.NONE):
            # LONG
            if (
                    (data['open'] > data['atr_up{}'.format(self.config['atr_ma'])])
            ):

                # todo open or close shift
                order = Order(time=time, symbol=symbol, side=const.OrderType.LONG, qty=balance / price, price=data['open'])

                orders.append(order)

        elif (position_side == const.Position.LONG):
            # SHORT
            if (
                    (price < data['ma{}'.format(self.config['ma'])])
                ):
                order = Order(time=time, symbol=symbol, side=const.OrderType.SHORT, qty=position_qty, price=data['open'])

                orders.append(order)

            #TAKE PROFIT
            elif (pct_unpnl > self.config['pct_tp']):
                order = Order(time=time, symbol=symbol, side=const.OrderType.SHORT, qty=position_qty, price=data['open'])

                orders.append(order)

            #STOP LOSS
            elif (pct_unpnl < self.config['pct_sl']):
                order = Order(time=time, symbol=symbol, side=const.OrderType.SHORT, qty=position_qty, price=data['open'])

                orders.append(order)

        return orders


def run_single(df_cds, config, nlargest, fee):
    df_cds_sub = df_cds.copy()

    ##STRATEGY##
    strategy = TTMStrategy()

    # get_indicators
    indicators = {'atr_mas' : [2,3,5,7,14,21,25,80,99]}
    df_cds_sub = strategy.get_indicators(df_cds_sub, indicators)

    #2list
    data_list = util.df2list_by_date_group(df_cds_sub)



    strategy.config = config

    ##ACCOUNT##
    INITIAL_BALANCE = 1e3
    initial_time = df_cds_sub.iloc[0]['time']
account = Account(initial_time, INITIAL_BALANCE)
##RUN BROKER##
    broker = Broker(account, data_list, strategy, fee)
    broker.run()
    final_balance = broker.account.wallet_balance
    print('final_balance:\n{}\n'.format(final_balance))


    pctcum = pct(INITIAL_BALANCE, final_balance)
    print('final_balance pctcum :\n{}\n'.format(pctcum))

    #result
    result, _, _ = broker.get_result_stats()
    return result




def run_opt(df_cds, fee, strategy_name, max_hold_symbols):
    ##
    df_cds_sub = df_cds.copy()
    print('len(df_cds_sub):\n{}\n'.format(len(df_cds_sub)))

    ##STRATEGY##
    strategy = TTMStrategy()

    # get_indicators
    indicators = {'atr_mas' : [2,3,5,7,14,21,25,80,99]}
    df_cds_sub = strategy.get_indicators(df_cds_sub, indicators)

    #2list
    data_list = util.df2list_by_date_group(df_cds_sub)

    ##OPTIMIZER##
    optimizer = Optimizer()

    #params
    params = {}

    #small range
    params['atr_ma'] = [2,3,5,7,14,21,25,80,99]
    params['ma'] = [2,3,5,7,14,21,25,80,99]

    params['pct_sl'] = np.arange(-0.01, 0, 0.005).tolist()
    params['pct_tp'] = np.arange(0, 0.01, 0.005).tolist()


    #test params
    # params['open_low'] = [1/100]
    # params['open_high'] = [2/100]
    #
    # params['close_low'] = [-2/100]
    # params['close_high'] = [-1/100]
    #
    # params['pct_sl'] = [-2/100]
    # params['pct_tp'] = [1/100]
    #
    # params['ema'] = [2, 3]

    optimizer.shuffle_params(params)
    param_batch = optimizer.get_cartesian_params(params)

    #run
    results = optimizer.optimize(data_list, strategy, param_batch, fee, strategy_name)
    print('len(opt_results):\n{}\n'.format(len(results)))
    df = pd.DataFrame(results)
    print(df.sort_values('pctcum')[-10:][['pctcum','sr','max_drawdown','num_trades']])



#FEE
LIMIT_FEE = 0.036 / 100
fee = LIMIT_FEE

frame = '1d'
frame = '1h'


exchange = "binance_spot"
symbols = ['ETH/USDT']

##READ##
df_cds = read(exchange, frame, symbols)
# df_cds = df_cds[df_cds['time'] > '2022-03-01']

# tops by volume
nlargest = 2
ma = 5
df_cds_tops = get_tops_by_vol(df_cds, ma, nlargest)

##RUN SINGLE##
config = {'atr_ma': 14, 'ma': 14, 'pct_tp': 1 / 100, 'pct_sl': -1 / 100}
result = run_single(df_cds_tops, config, nlargest, fee)
print('run_single result:\n{}\n'.format(result))

#RUN OPT
strategy_name = 'ttm'
run_opt(df_cds, fee, strategy_name, max_hold_symbols)
