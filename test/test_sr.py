# method 1: fractal candlestick pattern
# determine bullish fractal
import numpy as np

import plotly.graph_objects as go
from plotly.subplots import make_subplots


def is_support(df, i):
    cond1 = df['low'][i] < df['low'][i - 1]
    cond2 = df['low'][i] < df['low'][i + 1]
    cond3 = df['low'][i + 1] < df['low'][i + 2]
    cond4 = df['low'][i - 1] < df['low'][i - 2]
    return (cond1 and cond2 and cond3 and cond4)


# determine bearish fractal
def is_resistance(df, i):
    cond1 = df['high'][i] > df['high'][i - 1]
    cond2 = df['high'][i] > df['high'][i + 1]
    cond3 = df['high'][i + 1] > df['high'][i + 2]
    cond4 = df['high'][i - 1] > df['high'][i - 2]
    return (cond1 and cond2 and cond3 and cond4)


# to make sure the new level area does not exist already
def is_far_from_level(value, levels, df):
    ave = np.mean(df['high'] - df['low'])
    return np.sum([abs(value - level) < ave for _, level in levels]) == 0


from bt_lite.data import data_reader

reader = data_reader.SqlReader()
exchange = 'binance_future'
# 1d
# df_cds = reader.read_ohlcv(exchange, '1d')


def get_levels(df):
    # a list to store resistance and support levels
    levels = []
    for i in range(2, df.shape[0] - 2):
        if is_support(df, i):
            low = df['low'][i]
            if is_far_from_level(low, levels, df):
                levels.append((i, low))
        elif is_resistance(df, i):
            high = df['high'][i]
            if is_far_from_level(high, levels, df):
                levels.append((i, high))
    return levels

df_cds = reader.read_ohlcv(exchange, '1d')
df_cds = df_cds[df_cds['symbol'].isin(['ETH/USDT', 'BTC/USDT'])]
df_cds = df_cds.reset_index(drop=True)
#get symbols
symbols = df_cds['symbol'].unique()


#todo
##TO DB##
##TO DB##
##TO DB##
##SAVE IT IN STRATEGY##
##SAVE IT IN STRATEGY##
##SAVE IT IN STRATEGY##
##SAVE IT IN STRATEGY##
levels_symbols_times = {}
for symbol in symbols:
    #df_cds_symbol
    df_cds_symbol = df_cds[df_cds['symbol'] == symbol]
    df_cds_symbol = df_cds_symbol .reset_index(drop=True)

    #upto i time looping
    for i in range(100, len(df_cds_symbol)):
        print(i)
        # upto i time
        df = df_cds_symbol[:i]

        #last time
        time_ = df.iloc[-1]

        #get levels of symbol upto time_
        levels = get_levels(df)


        #save the levels of symbol at time
        levels_symbols_times['ETH/USDT_20220101_1d'] = levels


levels = levels_symbols_times['ETH/USDT_20220101']



#
# from mplfinance.original_flavor import candlestick_ohlc
# import matplotlib.dates as mpl_dates
# import matplotlib.pyplot as plt
#
#
#
# def plot_all(levels, df):
#     fig, ax = plt.subplots(figsize=(16, 9))
#     candlestick_ohlc(ax, df.values, width=0.6, colorup='green',
#                      colordown='red', alpha=0.8)
#     date_format = mpl_dates.DateFormatter('%d %b %Y')
#     ax.xaxis.set_major_formatter(date_format)
#     for level in levels:
#         plt.hlines(level[1], xmin=df['Date'][level[0]], xmax=
#         max(df['Date']), colors='blue', linestyle='--')
#     fig.show()



fig = make_subplots(rows=1, cols=1, specs=[ [{}], ], shared_xaxes=True, subplot_titles=("First Subplot", "Second Subplot"))

fig1 = go.Candlestick(x=df_cds['time'], open=df_cds['open'], high=df_cds['high'], low=df_cds['low'], close=df_cds['close'])
fig.add_trace(fig1, row=1, col=1)

for level in levels:
    level = level[1]
    # fig2 = go.Scatter(x=df_cds['time'], open=df_cds['open'], high=df_cds['high'], low=df_cds['low'], close=df_cds['close'])
    fig.add_hline(y=level)
    # fig.add_trace(fig2, row=1, col=1)

fig.update_layout(showlegend=False, title_text="Specs with Subplot Title", xaxis_rangeslider_visible=False)

fig.show()
