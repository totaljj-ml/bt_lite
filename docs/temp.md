        # closing bar : only close possible
        else:
            # close condition
            if (data_symbol_time['close'] < data_symbol_time['ema']):
                qty = quote_balance * lev / split / price
                orders.append(LONG, price, qty)

            # split close
            elif (unpnl_pct > split_close_pct):
                qty = quote_balance * lev / split / price
                orders.append(LONG, price, qty)
                
