
from bt_lite.core import Order, Broker, Account, Optimizer
from test.test_util import read, get_tops_by_vol

import pandas as pd
import numpy as np

from bt_lite.common import const, util, graph, graph_multi
from bt_lite.common.util import pct, divide_chunks
from bt_lite.indicators import moving_average, rsi, tops
from bt_lite.strategy import Strategy

def run_opt(df_cds, fee, nlargest, strategy_name):
    ##
    df_cds_sub = df_cds.copy()
    print('len(df_cds_sub):\n{}\n'.format(len(df_cds_sub)))

    ##STRATEGY##
    strategy = MaStrategy()

    # get_indicators
    indicators = { 'emas': [2,3,5,7,14,21,25,50,99], 'rsi_periods': [7, 14] }
    df_cds_sub = strategy.get_indicators(df_cds_sub, indicators)

    #2list
    data_list = util.df2list_by_date_group(df_cds_sub)

    ##OPTIMIZER##
    optimizer = Optimizer()

    #params
    params = {}

    #large range
    emas = [3, 5, 7, 14, 99]
    # emas = [2, 3, 5, 7, 14, 99]
    # emas = [25, 7 ,99]
    emas = [3]
    num_buckets = 5
    for ema in emas:
        print('-------------')
        print('ema:\n{}\n'.format(ema))
        params['ema'] = [ema]
        # params['lev'] = [1,2,3,4]
        params['lev'] = [1]
        # params['lev'] = [6,10,12]
        params['num_splits'] = [2]
        params['max_hold_symbols'] = [max_hold_symbols]

        # todo run_single 등으로 graph 확인
        #2701 백 찾았음 lev 10배?(2022_03_26__18_59_01_ma_2701 ,src 확인)

        #
        min_ = df_cds_sub['pct_ema{}_close_s1'.format(ema)].min()
        max_ = df_cds_sub['pct_ema{}_close_s1'.format(ema)].max()
        q1 = df_cds_sub['pct_ema{}_close_s1'.format(ema)].quantile(0.1)
        q2 = df_cds_sub['pct_ema{}_close_s1'.format(ema)].quantile(0.2)
        q3 = df_cds_sub['pct_ema{}_close_s1'.format(ema)].quantile(0.3)
        q4 = df_cds_sub['pct_ema{}_close_s1'.format(ema)].quantile(0.4)
        q5 = df_cds_sub['pct_ema{}_close_s1'.format(ema)].quantile(0.5)
        q6 = df_cds_sub['pct_ema{}_close_s1'.format(ema)].quantile(0.6)
        q7 = df_cds_sub['pct_ema{}_close_s1'.format(ema)].quantile(0.7)
        q8 = df_cds_sub['pct_ema{}_close_s1'.format(ema)].quantile(0.8)
        q9 = df_cds_sub['pct_ema{}_close_s1'.format(ema)].quantile(0.9)


        #
        size = (q6 - 0)/num_buckets
        print('q6:\n{}\n'.format(q6))
        params['open_low'] = np.arange(0, q6, size).tolist()
        params['open_high'] = np.arange(0, q6, size).tolist()

        size = (0 - q4)/num_buckets
        print('q4:\n{}\n'.format(q4))
        params['close_low'] = np.arange(q4, 0, size).tolist()
        params['close_high'] = np.arange(q4, 0, size).tolist()

        params['long1_pnl_pct'] = np.arange(-20/100, -10/100, 5/100).tolist()
        params['long2_pnl_pct'] = [0]

        params['pct_sl'] = [-1/100]
        params['pct_tp'] = np.arange(0, 10/100, 2/100).tolist()

        optimizer.shuffle_params(params)
        param_batch = optimizer.get_cartesian_params(params)

        #run
        results = optimizer.optimize(data_list, strategy, param_batch, fee, strategy_name)
        df = pd.DataFrame(results)
        print(df.sort_values('pctcum')[-10:][['pctcum','sr','max_drawdown','num_trades']])

class MaStrategy(Strategy):

    def __init__(self):
        super().__init__()
        self.config = None
        self.num_long = 0

    def get_indicators(self, data, indicators):
        df_cds = data

        #shift ohlc
        cols_shift = ['open', 'high', 'low', 'close']
        for col_shift in cols_shift:
            util.shift(df_cds, col_shift, 1)

        ##ma##
        emas = indicators['emas']
        sfts = [1]

        moving_average.set_ema(df_cds, emas, sfts)
        moving_average.set_pct_ema_close(df_cds, emas, sfts)

        ##rsi#
        #todo
        rsi_periods = indicators['rsi_periods']
        for rsi_period in rsi_periods:
            rsi.set_rsi(df_cds, rsi_period)

        return df_cds

    def execute(self, state):

        #orders
        orders = []

        #1d
        # df_cds1d = self.df_cds1d

        #account
        account = state['account']

        #data
        data = state['data']
        time = data['time']
        symbol = data['symbol']
        is_open_bar = data['is_open_bar']

        #TEST
        # if(symbol != 'ETH/USDT'):
        #     return orders

        #balance
        margin_balance = account.margin_balance
        margin_balance_low = account.margin_balance_low
        if(margin_balance < 0): return orders

        #liquidiated
        if(margin_balance_low  < 0):
            # print('margin_balance_low < 0')
            return orders

        #POSITION
        position = account.positions[symbol]
        position_side = position.side
        position_qty = position.qty

        #leverage
        lev = self.config['lev']

        #num_splits
        num_splits = self.config['num_splits']

        #max hold symbols
        max_hold_symbols = self.config['max_hold_symbols']

        #todo 다른 strategy 도
        #todo
        #margin_balance_at_open

        #todo
        #todo
        #todo
        #todo
        #todo
        #margin_balance_at_open to max_balance_position, change Position class
        #margin_balance_at_open to max_balance_position, change Position class
        #margin_balance_at_open to max_balance_position, change Position class
        #margin_balance_at_open to max_balance_position, change Position class
        #margin_balance_at_open to max_balance_position, change Position class
        #margin_balance_at_open to max_balance_position, change Position class
        #margin_balance_at_open to max_balance_position, change Position class
        #margin_balance_at_open to max_balance_position, change Position class
        #margin_balance_at_open to max_balance_position, change Position class
        #margin_balance_at_open to max_balance_position, change Position class
        margin_balance_at_open = position.margin_balance_at_open if(position.margin_balance_at_open) else account.margin_balance
        max_balance_position = margin_balance_at_open / max_hold_symbols

        #pct unpnl
        pct_unpnl = 0
        if(position.side != const.Position.NONE):
            pct_unpnl = pct(max_balance_position, max_balance_position + position.unrealized_pnls[-1])

        ##이격도##
        criteria_col = 'pct_ema{}_close_s1'.format(self.config['ema'])
        criteria = data[criteria_col]

        if(is_open_bar):
            if (position_side == const.Position.NONE):
                # OPEN LONG
                if (
                        (data['price'] > data['ema99_s1']) &
                        (criteria > self.config['open_low']) &
                        (criteria < self.config['open_high'])
                ):
                    #todo check
                    qty = max_balance_position * lev / num_splits / max_hold_symbols / data['price']

                    # todo open or close shift
                    order = Order(time=time, symbol=symbol, side=const.OrderType.LONG, qty=qty, price=data['price'])
                    orders.append(order)
                    self.num_long += 1
            # OPEN SPLIT
            elif (
                    (position_side == const.Position.LONG) &
                    (num_splits > 1) &
                    (self.num_long == 1) &
                    (pct_unpnl < self.config['long1_pnl_pct'])
            ):
                qty = max_balance_position * lev / num_splits / max_hold_symbols / data['price']
                order = Order(time=time, symbol=symbol, side=const.OrderType.LONG, qty=qty, price=data['price'])
                orders.append(order)
                self.num_long += 1

            # elif (
            #         (self.num_long == 2) &
            #         (pct_unpnl > self.config['long2_pnl_pct'])
            # ):
            #     order = Order(time=time, symbol=symbol, side=const.OrderType.SHORT, qty=position_qty, price=data['price'])
            #     orders.append(order)
            #     self.num_long += 1

        else:
            if (position_side == const.Position.LONG):
                # CLOSE LONG(CONDITION)
                if (
                        (criteria > self.config['close_low']) &
                        (criteria < self.config['close_high'])
                ):
                    order = Order(time=time, symbol=symbol, side=const.OrderType.SHORT, qty=position_qty, price=data['price'])
                    orders.append(order)
                    self.num_long = 0

                #CLOSE_LONG(TAKE PROFIT)
                elif (
                        pct_unpnl > self.config['pct_tp']
                ):
                    order = Order(time=time, symbol=symbol, side=const.OrderType.SHORT, qty=position_qty, price=data['price'])
                    orders.append(order)
                    self.num_long = 0



        return orders

def run_single(df_cds, config, nlargest, fee):
    df_cds_sub = df_cds.copy()

    ##STRATEGY##
    strategy = MaStrategy()

    # get_indicators
    indicators = { 'emas': [2,3,5,7,14,21,25,99], 'rsi_periods':[7,14] }
    df_cds_sub = strategy.get_indicators(df_cds_sub, indicators)

    #2list
    data_list = util.df2list_by_date_group(df_cds_sub)

    #config
    strategy.config = config

    ##ACCOUNT##
    INITIAL_BALANCE = 1e3
    initial_time = df_cds_sub.iloc[0]['time']
    account = Account(initial_time, INITIAL_BALANCE)

    ##RUN BROKER##
    broker = Broker(account, data_list, strategy, fee)
    broker.run()
    final_balance = broker.account.margin_balance
    print('final_balance:\n{}\n'.format(final_balance))

    pctcum = pct(INITIAL_BALANCE, final_balance)
    print('final_balance pctcum :\n{}\n'.format(pctcum))

    ##get_trades##
    df_trades = broker.get_trades()
    df_trades.to_csv('{}/test.csv'.format(const.ROOT_DIR), index=False)

    # debug
    # cols = ['time', 'price', 'open_price', 'open_qty', 'side', 'realized_pnl', 'pct', 'pct_margin']
    cols = ['time', 'price', 'side', 'pct', 'pct_margin', 'margin_balance']
    print('df_trades:\n{}\n'.format(df_trades[cols]))
    df_trades.to_csv('{}/test.csv'.format(const.ROOT_DIR), index=False)

    df_trades_close = df_trades[df_trades['side'] == -1]
    df_trades_close['pct1'] = df_trades_close['pct'] + 1
    df_trades_close['pctcum'] = df_trades_close['pct1'].cumprod()
    print('df_trades_close:\n{}\n'.format(df_trades_close[cols]))


    #result
    result, _, _ = broker.get_result_stats()
    print('run_single result:\n{}\n'.format(result))

    ##draw##
    graph.draw(df_cds, df_trades)


    return broker

if __name__ == '__main__':

    #FEE
    # LIMIT_FEE = 0.036 / 100
    LIMIT_FEE = 0
    fee = LIMIT_FEE

    #FRAMES
    frame = '1d'
    # frame = '1h'
    # symbols = ['ETH/USDT', 'LUNA/USDT']
    symbols = ['ETH/USDT']
    # symbols = ['LUNA/USDT']

    # exchange = 'binance_spot'
    exchange = 'binance_future'

    ##READ##
    df_cds = read(exchange, frame, symbols)
    # df_cds = df_cds[df_cds['time'] > '2021-01-22']

    #TEST
    # df_cds = df_cds[df_cds['time'] > '2020-01-10']
    # df_cds = df_cds[df_cds['time'] > '2021-03-15']

    df_cds = df_cds.reset_index(drop=True)

    #tops by volume 1d
    nlargest = len(symbols)
    max_hold_symbols = nlargest
    ma = 1
    df_cds_tops = get_tops_by_vol(df_cds, ma, nlargest)

    print('df_cds range:{}~{}'.format(df_cds.iloc[0]['time'], df_cds.iloc[-1]['time']))
    print('df_cds_tops range:{}~{}'.format(df_cds_tops.iloc[0]['time'], df_cds_tops.iloc[-1]['time']))
    # df_cds.to_csv('{}/data/test/test.csv'.format(const.ROOT_DIR), index=False)
    # df_cds_tops.to_csv('{}/data/test/test_tops.csv'.format(const.ROOT_DIR), index=False)

    #repeat to loop the bar two times(at open and close)
    df_cds_tops = util.split_to_open_close(df_cds_tops)

    ##RUN SINGLE##
    config = {
         # 'lev': 6,
         'lev': 1,
         # 'num_splits': 1,
         # 'max_hold_symbols': max_hold_symbols,

        # todo
        # btc/eth 100배, mdd -10, 3달치 graph 확인, worst margnin low 확인,
         'num_splits': 1,
         'max_hold_symbols': max_hold_symbols,

         'ema': 3,
         'open_low': 0.0, 'open_high': 0.01,
         'close_low': -0.02, 'close_high': -0.02,

         'long1_pnl_pct': -0.10, 'long2_pnl_pct': -0.10,

         'pct_sl': -0.01, 'pct_tp': 0.01,
     }

    #LUNA
    config = {
         'lev': 2,
         'num_splits': 1,
         'max_hold_symbols': max_hold_symbols,

         'ema': 2,
         'open_low': 0.0, 'open_high': 0.05,
         'close_low': -100.02, 'close_high': -90.02,

         'long1_pnl_pct': -0.10, 'long2_pnl_pct': -0.10,

         'pct_sl': -0.01, 'pct_tp': 0.10,
     }
    #scenario , lev, split
    #scenario , lev, split
    #scenario , lev, split
    #scenario , lev, split
    #scenario , lev, split
    #scenario , lev, split
    #scenario , lev, split
    #scenario , lev, split
    print(config)

    #run
    broker = run_single(df_cds_tops, config, nlargest, fee)

    ##RUN OPT##
    # strategy_name = 'ma_grid_search'
    # run_opt(df_cds_tops, fee, nlargest, strategy_name)

    #atr 이 잘 나왔음.. okshin, chartist 보기

