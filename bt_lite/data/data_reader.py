
from sqlalchemy import create_engine
import pandas as pd

from bt_lite.common import db
from bt_lite.common.const import DB


class SqlReader:

    def __init__(self):
        # self.engine = create_engine("mysql+mysqldb://bt_lite:"+"bt_lite1"+"@localhost/etherscan", encoding='utf-8')
        self.engine = db.get_engine()

    def _read_sql(self, sql):
        conn = self.engine.connect()
        # df_cds_f = pd.read_sql('select * from candle_ft', con=conn)
        df = pd.read_sql(sql, con=conn)
        conn.close()
        return df

    def read_ohlcv(self, exchange, frame):
        sql = 'select * from cds where exchange = "{}" and frame = "{}"'.format(exchange, frame)
        df_cds = self._read_sql(sql)

        df_cds['sym_quote'] = df_cds['symbol'].str.split('-').str[0]
        df_cds['sym_base'] = df_cds['symbol'].str.split('-').str[1]

        # if(quote_vol null)
        # df_cds['vol'] = df_cds['base_vol'] * df_cds['close'] # bth only

        df_cds['volume'] = df_cds['quote_vol']
        df_cds['vol'] = df_cds['quote_vol']
        
        ##time##
        df_cds['time'] = pd.to_datetime(df_cds['time'], format='%Y-%m-%d %H:%M:%S')
        df_cds['month'] = df_cds['time'].dt.strftime('%Y-%m')
        df_cds['week'] = df_cds['time'].dt.strftime('%Y-%W')

        df_cds = df_cds.sort_values(['symbol','time'])
        df_cds = df_cds.drop_duplicates(['symbol','time'])
        df_cds = df_cds.reset_index(drop=True)

        return df_cds



# def test():
#
#     reader = SqlReader()
#     df_cds = reader.read_ohlcv('binance_spot', '1d')
    # print('df_cds range:{}~{}'.format(df_cds.iloc[0]['time'], df_cds.iloc[-1]['time']))