import datetime

from bt_lite.data import data_reader
from bt_lite.indicators import moving_average, tops


def read(exchange, frame, symbols):
    ###READ####
    reader = data_reader.SqlReader()

    # 1d
    df_cds = reader.read_ohlcv(exchange, frame)

    # df_cds = reader.read_ohlcv('binance_future', '1h')
    # merge
    # df_cds = pd.merge(df_cds1d, df_cds15m, on=['symbol', 'date'], suffixes=['1d', '15m'])
    # df_cds = df_cds15m


    if(symbols):
        # df_cds = df_cds[df_cds['symbol'] == symbol]
        df_cds = df_cds[df_cds['symbol'].isin(symbols)]

    df_cds = df_cds.reset_index(drop=True)

    return df_cds


def resample_cds(df_cds, resample_rule):
    df_cds_re = df_cds.set_index('time').groupby('symbol').resample(resample_rule).agg({'open': 'first',
                                                                                        'high': 'max',
                                                                                        'low': 'min',
                                                                                        'close': 'last'}).reset_index()
    df_cds_re['date'] = df_cds_re['time'].dt.date
    return df_cds_re




def get_tops_by_vol(df_cds, ma, nlargest):
    #tops by volume 1d
    # ma = 5
    moving_average.set_volume_ma(df_cds, ma)
    df_cds_tops = tops.get_nlargest(df_cds, 'vol_ma{}_s1'.format(ma), nlargest)
    return df_cds_tops
