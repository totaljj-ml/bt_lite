
#atr 이 잘 나왔음.. okshin, chartist 보기

from bt_lite.core import Order, Broker, Account, Optimizer
from test.test_util import read

import pandas as pd
import numpy as np

from bt_lite.common import const, util
from bt_lite.common.util import pct
from bt_lite.indicators import rsi
from bt_lite.strategy import Strategy

class RSISeparationStrategy(Strategy):

    def __init__(self):
        super().__init__()
        self.config = None
        self.num_long = 0

    def get_indicators(self, data, indicators):
        df_cds = data

        #shift ohlc
        cols_shift = ['open', 'high', 'low', 'close']
        for col_shift in cols_shift:
            util.shift(df_cds, col_shift, 1)

        ##ma##
        # emas = indicators['emas']
        # sfts = [1]
        # moving_average.set_ema(df_cds, emas, sfts)
        # moving_average.set_pct_ema_close(df_cds, emas, sfts)

        ##rsi#
        rsi_periods = indicators['rsi_periods']
        for rsi_period in rsi_periods:
            rsi.set_rsi(df_cds, rsi_period)

        return df_cds

    def execute(self, state):
        orders = []

        account = state['account']
        data = state['data']

        wallet_balance = account.wallet_balance
        margin_balance = account.margin_balance
        

        position_side = account.position.side
        position_qty = account.position.qty

        time = data['time']

        if(pct_unpnl < -1):
            print(pct_unpnl, time)


        ##이격도##
        num_splits = 3


        #debug
        #debug
        #debug
        # print(time, pct_unpnl, price, account.position.avg_price)

        lev = self.config['lev']
        qty = wallet_balance * lev / num_splits / price

        if (position_side == const.Position.NONE):
            # LONG
            if (
                    (data['rsi{}_s1'.format(self.config['rsi_period'])] < self.config['open_rsi'])
            ):

                # todo open or close shift
                order = Order(time=time, symbol=symbol, side=const.OrderType.LONG, qty=qty, price=data['open'])
                orders.append(order)
                self.num_long += 1

        elif (position_side == const.Position.SHORT):
            # LONG
            if (
                    (num_splits > 1) &
                    (self.num_long == 1) &
 (pct_unpnl < self.config['short1_pnl_pct'])
            ):
                qty = wallet_balance_at_open * lev / num_splits / max_hold_symbols / data['open']
                order = Order(time=time, symbol=symbol, side=const.OrderType.LONG, qty=position_qty, price=data['open'])
                orders.append(order)
                self.num_long += 1
            elif (
                    (num_splits > 2) &
                    (self.num_long == 2) &
 (pct_unpnl < self.config['long2_pnl_pct'])
            ):

                qty = wallet_balance_at_open * lev / num_splits / max_hold_symbols / data['open']
                order = Order(time=time, symbol=symbol, side=const.OrderType.LONG, qty=position_qty, price=data['open'])
                orders.append(order)
                self.num_long += 1


        elif (position_side == const.Position.LONG):
            # LONG
            if (
                    (self.num_long  == 1) &
                    (pct_unpnl < self.config['long1_pnl_pct'])
            ):
                order = Order(time=time, symbol=symbol, side=const.OrderType.LONG, qty=qty, price=data['open'])
                orders.append(order)
                self.num_long += 1
            elif (
                    (self.num_long  == 2) &
                    (pct_unpnl < self.config['long2_pnl_pct'])
            ):
                order = Order(time=time, symbol=symbol, side=const.OrderType.LONG, qty=qty, price=data['open'])
                orders.append(order)
                self.num_long += 1

            # elif (
            #         (self.num_long == 3) &
            #         (pct_unpnl < self.config['pct_sl'])
            # ):
            #     order = Order(time=time, symbol=symbol, side=const.OrderType.SHORT, qty=position_qty, price=data['open'])
            #     orders.append(order)
            #     self.num_long = 0

            #TAKE PROFIT
            elif (pct_unpnl > self.config['pct_tp']):
                order = Order(time=time, symbol=symbol, side=const.OrderType.SHORT, qty=position_qty, price=data['open'])
                orders.append(order)
                self.num_long = 0

            #STOP LOSS
            # elif (pct_unpnl < self.config['pct_sl']):
            #     order = Order(
            #         # type=certis.OrderType.MARKET,
            #         time=time,
            #         side=const.OrderType.SHORT,
            #         qty=position_qty,
            #         price=data['open'],
            #     )
            #     # print('short:\n{}\n'.format(data))
            #     orders.append(order)

        return orders

def run_single(df_cds, config, nlargest, fee):
    df_cds_sub = df_cds.copy()

    ##STRATEGY##
    strategy = RSISeparationStrategy()

    # get_indicators
    indicators = {
        'rsi_periods':[7,14]
    }
    df_cds_sub = strategy.get_indicators(df_cds_sub, indicators)
    # df_cds_sub = df_cds_sub[df_cds_sub['time'] > '2021-05-16']
    data_list = util.df2list(df_cds_sub)


    config = {
        #todo 청산 ㅠ, timecut?
        #param 조절
        #opt 할때 청산 되면 빼기?
        #opt 할때 청산 되면 빼기?

        'lev' : 3,
        'rsi_period' : 14, 'open_rsi' : 60,
        'long1_pnl_pct' : -30/100, 'long2_pnl_pct' : -50/100,
        'pct_tp': 15 / 100
         # 'pct_tp': 2/100, 'pct_sl':-1/100
    }

    # 'num_sep': 5,
    # 'num_sep': 5,
    # unpnl -1 날짜 값 확인
    # unpnl -1 날짜 값 확인

    strategy.config = config

    ##ACCOUNT##
    INITIAL_BALANCE = 1e3
    initial_time = df_cds_sub.iloc[0]['time']
account = Account(initial_time, INITIAL_BALANCE)
##RUN BROKER##
    broker = Broker(account, data_list, strategy, fee)
    broker.run()
    final_balance = broker.account.wallet_balance
    print('final_balance:\n{}\n'.format(final_balance))


    pctcum = pct(INITIAL_BALANCE, final_balance)
    print('final_balance pctcum :\n{}\n'.format(pctcum))

    return broker




def run_opt(df_cds, fee, strategy_name, max_hold_symbols):
    ##
    df_cds_sub = df_cds.copy()
    print('len(df_cds_sub):\n{}\n'.format(len(df_cds_sub)))

    ##STRATEGY##
    strategy = RSISeparationStrategy()

    # get_indicators
    indicators = {
        'emas': [2,3,5,7,14,21,25,99],
        'rsi_periods': [7, 14]
    }
    df_cds_sub = strategy.get_indicators(df_cds_sub, indicators)

    #2list
    data_list = util.df2list_by_date_group(df_cds_sub)

    ##OPTIMIZER##
    optimizer = Optimizer()

    #params
    params = {}

    #small range
    # params['open_low'] = np.arange(0, 0.01, 0.005).tolist()
    # params['open_high'] = np.arange(0, 0.01, 0.005).tolist()
    #
    # params['close_low'] = np.arange(-0.01, 0, 0.005).tolist()
    # params['close_high'] = np.arange(-0.01, 0, 0.005).tolist()
    #
    # params['pct_sl'] = np.arange(-0.01, 0, 0.005).tolist()
    # params['pct_tp'] = np.arange(0, 0.01, 0.005).tolist()
    #
    # params['ema'] = [2,3,5,7,14]

    #large range
    params['ema'] = [2,3,5,7,14,99]
    params['open_low'] = np.arange(0, 0.02, 0.01).tolist()
    params['open_high'] = np.arange(0, 0.02, 0.01).tolist()
    params['close_low'] = np.arange(-0.02, 0, 0.01).tolist()
    params['close_high'] = np.arange(-0.02, 0, 0.01).tolist()

    params['rsi_period'] = [7, 14]
    params['open_rsi'] = [70, 80]

    params['pct_sl'] = np.arange(-0.02, 0, 0.01).tolist()
    params['pct_tp'] = np.arange(0, 0.02, 0.01).tolist()


    #test params
    # params['open_low'] = [1/100]
    # params['open_high'] = [2/100]
    #
    # params['close_low'] = [-2/100]
    # params['close_high'] = [-1/100]
    #
    # params['pct_sl'] = [-2/100]
    # params['pct_tp'] = [1/100]
    #
    # params['ema'] = [2, 3]

    optimizer.shuffle_params(params)
    param_batch = optimizer.get_cartesian_params(params)

    #run
    results = optimizer.optimize(data_list, strategy, param_batch, fee, strategy_name)
    df = pd.DataFrame(results)
    print(df.sort_values('pctcum')[-10:][['pctcum','sr','max_drawdown','num_trades']])


#FEE
LIMIT_FEE = 0.036 / 100
fee = LIMIT_FEE


frame = '1h'
# frame = '1d'

df_cds = read(exchange, frame, symbols)
print('df_cds range:{}~{}'.format(df_cds.iloc[0]['time'], df_cds.iloc[-1]['time']))

# df_cds = df_cds[df_cds['time'] > '2021-05-01']


broker = run_single(df_cds_tops, config, nlargest, fee)
result, _, _ = broker.get_result_stats()
print('run_single result:\n{}\n'.format(result))

# run_opt(df_cds, fee, strategy_name, max_hold_symbols)


#todo
#값확인
#wallet_balance pnl?
#param 바꾸면서



