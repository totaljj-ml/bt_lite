
from bt_lite.core import Order, Broker, Account, Optimizer
from test.test_util import read, get_tops_by_vol

import pandas as pd
import numpy as np

from bt_lite.common import const, util
from bt_lite.common.util import pct, divide_chunks
from bt_lite.indicators import moving_average, rsi
from bt_lite.strategy import Strategy

def run_opt(df_cds, fee, strategy_name, max_hold_symbols):
    ##
    df_cds_sub = df_cds.copy()
    print('len(df_cds_sub):\n{}\n'.format(len(df_cds_sub)))

    ##STRATEGY##
    strategy = MaLowStrategy()

    # get_indicators
    indicators = {
        'emas': [2,3,5,7,14,21,25,99],
        'rsi_periods': [7, 14],
    }
    df_cds_sub = strategy.get_indicators(df_cds_sub, indicators)

    #2list
    data_list = util.df2list_by_date_group(df_cds_sub)

    ##OPTIMIZER##
    optimizer = Optimizer()

    #params
    params = {}


    #large range
    params['ema'] = [7,25,99]
    params['lev'] = [10, 15, 20]
    params['num_splits'] = [2,3]
    params['max_hold_symbols'] = [max_hold_symbols]
    params['open_lt'] = np.arange(-10/100, -0/100, 1/100).tolist()
    params['long1_pnl_pct'] = np.arange(-10/100, -1/100, 1/100).tolist()
    # params['long2_pnl_pct'] = np.arange(-40/100, -20/100, 5/100).tolist()
    params['pct_sl'] = np.arange(-10/100, 0, 1/100).tolist()
    params['pct_tp'] = np.arange(0, 10/100, 1/100).tolist()


    optimizer.shuffle_params(params)
    param_batch = optimizer.get_cartesian_params(params)

    #run
    results = optimizer.optimize(data_list, strategy, param_batch, fee, strategy_name)
    df = pd.DataFrame(results)
    print(df.sort_values('pctcum')[-10:][['pctcum','sr','max_drawdown','num_trades']])

class MaLowStrategy(Strategy):

    def __init__(self):
        super().__init__()
        self.config = None
        self.num_long = 0

    def get_indicators(self, data, indicators):
        df_cds = data


        #shift ohlc
        cols_shift = ['open', 'high', 'low', 'close']
        for col_shift in cols_shift:
            util.shift(df_cds, col_shift, 1)

        ##ma##
        emas = indicators['emas']
        sfts = [1]

        moving_average.set_ema(df_cds, emas, sfts)
        moving_average.set_pct_ema_close(df_cds, emas, sfts)

        ##rsi#
        #todo
        rsi_periods = indicators['rsi_periods']
        for rsi_period in rsi_periods:
            rsi.set_rsi(df_cds, rsi_period)

        return df_cds

    def execute(self, state):

        #orders
        orders = []

        #1d
        # df_cds1d = self.df_cds1d

        #account
        account = state['account']

        #data
        data = state['data']
        time = data['time']
        symbol = data['symbol']

        #balance
        wallet_balance = account.wallet_balance
        margin_balance_low = account.margin_balance_low
        if(wallet_balance < 0): return orders
        if(margin_balance_low < 0):
            # print('wallet_balance_low < 0')
            return orders

        #position
        position = account.positions[symbol]
        position_side = position.side
        position_qty = position.qty
        wallet_balance_at_open = position.wallet_balance_at_open
        pct_unpnl = position.unrealized_pnl

        #leverage
        lev = self.config['lev']

        #num_splits
        num_splits = self.config['num_splits']

        #max_hold_symbols
        max_hold_symbols= self.config['max_hold_symbols']

        ##이격도##
        criteria_col = 'pct_ema{}_close_s1'.format(self.config['ema'])
        criteria = data[criteria_col]
        if (position_side == const.Position.NONE):
            # OPEN LONG
            if (
                    (data['open'] > data['ema99_s1']) &
                    (criteria < self.config['open_lt'])
            ):
                qty = wallet_balance * lev / num_splits / max_hold_symbols / data['open']

                # todo open or close shift
                order = Order(time=time, symbol=symbol, side=const.OrderType.LONG, qty=qty, price=data['open'])
                orders.append(order)
                self.num_long += 1


        elif (position_side == const.Position.LONG):
            # # CLOSE LONG(CONDITION)
            # if (
            #         (criteria > self.config['close_low']) &
            #         (criteria < self.config['close_high'])
            # ):
            #     order = Order(time=time, symbol=symbol, side=const.OrderType.SHORT, qty=position_qty, price=data['open'])
            #     orders.append(order)
            #     self.num_long = 0

            #CLOSE_LONG(TAKE PROFIT)
            if (
                    pct_unpnl > self.config['pct_tp']
            ):
                order = Order(time=time, symbol=symbol, side=const.OrderType.SHORT, qty=position_qty, price=data['open'])
                orders.append(order)
                self.num_long = 0
            if (
                    pct_unpnl < self.config['pct_sl']
            ):
                order = Order(time=time, symbol=symbol, side=const.OrderType.SHORT, qty=position_qty, price=data['open'])
                orders.append(order)
                self.num_long = 0

            #OPEN SPLIT
            elif (
                    (num_splits > 1) &
                    (self.num_long == 1) &
 (pct_unpnl > self.config['long1_pnl_pct'])
            ):

                qty = wallet_balance_at_open * lev / num_splits / max_hold_symbols / data['open']
                order = Order(time=time, symbol=symbol, side=const.OrderType.LONG, qty=qty, price=data['open'])
                orders.append(order)
                self.num_long += 1

            # elif (
            #         (self.num_long == 2) &
            #         (pct_unpnl > self.config['long2_pnl_pct'])
            # ):
            #     order = Order(time=time, symbol=symbol, side=const.OrderType.LONG, qty=position_qty, price=data['open'])
            #     orders.append(order)
            #     self.num_long += 1


        return orders

def run_single(df_cds, config, nlargest, fee):
    df_cds_sub = df_cds.copy()

    ##STRATEGY##
    strategy = MaLowStrategy()

    # get_indicators
    indicators = { 'emas': [2,3,5,7,14,21,25,99], 'rsi_periods':[7,14], }
    df_cds_sub = strategy.get_indicators(df_cds_sub, indicators)

    #2list
    data_list = util.df2list_by_date_group(df_cds_sub)

    strategy.config = config

    ##ACCOUNT##
    INITIAL_BALANCE = 1e3
    initial_time = df_cds_sub.iloc[0]['time']
    account = Account(initial_time, INITIAL_BALANCE)

    ##RUN BROKER##
    broker = Broker(account, data_list, strategy, fee)
    broker.run()
    final_balance = broker.account.wallet_balance
    print('final_balance:\n{}\n'.format(final_balance))



    #result
    result, _, _ = broker.get_result_stats()
    return result

if __name__ == '__main__':

    #FEE
    LIMIT_FEE = 0.036 / 100
    fee = LIMIT_FEE

    #FRAMES
    # frame = '1h'
    frame = '1d'
    symbols = ['ETH/USDT', 'BTC/USDT']
    exchange = 'binance_future'

    ##READ##
    df_cds = read(exchange, frame, symbols)
    df_cds = df_cds[df_cds['time'] > '2021-07-01']
    df_cds = df_cds.reset_index(drop=True)
    print('df_cds range:{}~{}'.format(df_cds.iloc[0]['time'], df_cds.iloc[-1]['time']))

    #tops by volume 1d
    nlargest = len(symbols)
    max_hold_symbols = nlargest
    ma = 5
    df_cds_tops = get_tops_by_vol(df_cds, ma, nlargest)

    ##RUN SINGLE##
    config = {
            'lev': 10,
            'num_splits': 3,
            'max_hold_symbols': max_hold_symbols,

            'ema': 99,
            'open_lt': -5/100,
            'long1_pnl_pct': -5 / 100, 'long2_pnl_pct': -30 / 100,
            'pct_sl': -0.01, 'pct_tp': 0.01,
     }

    result = run_single(df_cds_tops, config, nlargest, fee)
    print('run_single result:\n{}\n'.format(result))

    ##RUN OPT##
    strategy_name = 'ma'
    run_opt(df_cds_tops, fee, strategy_name, max_hold_symbols)



