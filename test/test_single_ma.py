import quantstats as qs

from bt_lite.common.util import divide_chunks
from bt_lite.core import Account, Broker
from bt_lite.indicators import moving_average, tops
from test.strategies.ma.ma_strategy_grid_search import MaStrategy
from test.test_util import read

qs.extend_pandas()

import pandas as pd

from bt_lite.common import util, const

###READ####
##STRATEGY##

df_cds = read('binance_future', '1d', ['ETH/USDT', 'BTC/USDT'])
# df_cds = df_cds[df_cds['time'] > '2021-07-01']
# df_cds = df_cds[df_cds['time'] > '2021-11-01']

# tops by volume
nlargest = 2
ma = 5
moving_average.set_volume_ma(df_cds, ma)
df_cds_tops = tops.get_nlargest(df_cds, 'vol_ma{}_s1'.format(ma), nlargest)
print('df_cds range:{}~{}'.format(df_cds.iloc[0]['time'], df_cds.iloc[-1]['time']))

#FEE
LIMIT_FEE = 0.036 / 100

strategy = MaStrategy()

# get_indicators
indicators = {'emas': [2, 3, 5, 7, 14, 21, 25, 99], 'rsi_periods': [7, 14], }
df_cds_sub = strategy.get_indicators(df_cds_tops, indicators)

#2list
data_list = util.df2list_by_date_group(df_cds_sub)
# d = pd.read_csv('{}/data/optimizer_results/2022_03_26__18_59_01_ma_2701/results.csv'.format(const.ROOT_DIR))
d = pd.read_csv('{}/data/optimizer_results/2022_03_29__18_09_23_ma_grid_search_203/results.csv'.format(const.ROOT_DIR))

# d = d[d['num_trades'] > 50]
d = d.sort_values('pctcum')
config = d.iloc[-1].to_dict()
config['ema'] = int(config['ema'])
strategy.config = config
print("strategy.config:{}".format(strategy.config))

##ACCOUNT##
INITIAL_BALANCE = 1e3
initial_time = df_cds_sub.iloc[0]['time']
account = Account(initial_time, INITIAL_BALANCE)
##RUN BROKER##
broker = Broker(account, data_list, strategy, LIMIT_FEE)
broker.run()
final_balance = broker.account.wallet_balance
print('final_balance : {}'.format(final_balance))

#
# print(pctcum * 100)

lev = 3
result, _, _ = broker.get_result_stats()
print('run_single result:\n{}\n'.format(result))


# draw
df_trades = broker.get_trades()
# df_cds = df_cds[df_cds['time'] > '2022-01-01']
# df_trades = df_trades[df_trades['time'] > '2022-01-01']
# #graph.draw(df_cds, df_trades)

#공유하기
#공유하기

#BTC 해보기
#lev 줄이기

# 21/09/12 이상함
#ema99 그리기
