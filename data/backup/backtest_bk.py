
import pandas as pd

class BacktestLoop:
    def run_(df_s):
        DEBUG =False

        df_orders = pd.DataFrame()
        df_orders = df_orders.append(df_s[df_s['open_long']])
        # df_orders = df_orders.append(df_s[df_s['long_tp']])
        df_orders = df_orders.append(df_s[df_s['close_long']])
        df_orders = df_orders.sort_values('time').reset_index(drop=True)
        def set_res(r, pos, df_ord):
            r['open_side'] = pos['side']
            r['open_time'] = pos['open_time']
            r['close_time'] = df_ord['time']
            r['open_price'] = pos['open_price']
            r['close_price'] = df_ord['open']
            return r


        pos = {'side':'none'} #open_price, open_time, side
        rs = [] #''
        for i, df_ord in df_orders.iterrows():
        #     if(i >30):break
            r = {}
            #order long
            if(df_ord['open_long']):
                #current no position
                if(pos['side'] == 'none'):
                    #open long
                    pos['side'] = 'long'
                    pos['open_price'] = df_ord['open']
                    pos['open_time'] = df_ord['time']
                    if(DEBUG):
                        print('open  : {}'.format(pos))
                        print('pos:\n{}\n'.format(pos))


            #order short
            elif(df_ord['close_long']):
                if(pos['side'] == 'long'):
                    #close long : res
                    set_res(r, pos, df_ord)
                    r['pnl'] = pct(r['open_price'], r['close_price'])
    #                 r['pnl'] = pct(r['close_price'], r['open_price'])
                    if(DEBUG):
                        print('closed res : {}'.format(r))

                    #close pos
                    pos['side'] = 'none'
                    if(DEBUG):
                        print('pos  : {}'.format(pos))

            if(len(r) == 0): continue
            if(DEBUG):
                print('r:{}\n'.format(r))
            rs.append(r)
        return rs

    DEBUG = True
    DEBUG = False
