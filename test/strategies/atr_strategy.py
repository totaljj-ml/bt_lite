import quantstats as qs

from bt_lite.core import Order, Broker, Account, Optimizer
from test.test_util import read, get_tops_by_vol

qs.extend_pandas()

import pandas as pd
import numpy as np

from bt_lite.common import const, util
from bt_lite.common.util import pct, divide_chunks
from bt_lite.indicators import atr, moving_average
from bt_lite.strategy import Strategy

class AtrStrategy(Strategy):
    def __init__(self):
        super().__init__()
        self.config = None

    #mas, atr_mas
    def get_indicators(self, data, indicators):
        df_cds = data

        #shift ohlc
        cols_shift = ['open', 'high', 'low', 'close']
        for col_shift in cols_shift:
            util.shift(df_cds, col_shift, 1)

        ##atr##
        atr_mas = indicators['atr_mas']
        sfts = [1]
        for atr_ma in atr_mas:
            moving_average.set_ma(df_cds, 'open', atr_ma)
            atr.set_atr(df_cds, atr_ma)

            col = 'atr_up{}'.format(atr_ma)
            df_cds[col] = df_cds['ma{}'.format(atr_ma)] + df_cds['atr{}'.format(atr_ma)] * 2
            util.shift(df_cds, col, 1)

            df_cds['atr_down{}'.format(atr_ma)] = df_cds['ma{}'.format(atr_ma)] - df_cds['atr{}'.format(atr_ma)] * 2

        return df_cds

    def execute(self, state):
        #orders
        orders = []

        #account
        account = state['account']

        #data
        data = state['data']
        time = data['time']
        symbol = data['symbol']

        #balance
        wallet_balance = account.wallet_balance
        margin_balance_low = account.margin_balance_low
        if(wallet_balance < 0): return orders
        if(margin_balance_low < 0):
            # print('wallet_balance_low < 0')
            return orders
        
        #position
        position = account.positions[symbol]
        position_side = position.side
        position_qty = position.qty
        pct_unpnl = position.unrealized_pnl

        ##이격도##
        lev = self.config['lev']

        #max hold symbols
        max_hold_symbols = self.config['max_hold_symbols']

        if (position_side == const.Position.NONE):
            # LONG
            if (
                    # (data['open'] > data['atr_up{}'.format(self.config['atr_ma'])])
                    (data['open_s1'] > data['atr_up{}_s1'.format(self.config['atr_ma'])])
            ):

                # todo open or close shift
                qty = wallet_balance * lev / max_hold_symbols / data['open']
                order = Order(time=time, symbol=symbol, side=const.OrderType.LONG, qty=qty,  price=data['open'])
                orders.append(order)

        elif (position_side == const.Position.LONG):
            # SHORT
            if (
                    (data['open'] < data['ma{}'.format(self.config['ma'])])
                ):
                order = Order(time=time, symbol=symbol, side=const.OrderType.SHORT, qty=position_qty, price=data['open'])
                orders.append(order)

            #TAKE PROFIT
            elif (pct_unpnl > self.config['pct_tp']):
                order = Order(time=time, symbol=symbol, side=const.OrderType.SHORT, qty=position_qty, price=data['open'])
                orders.append(order)

            #STOP LOSS
            elif (pct_unpnl < self.config['pct_sl']):
                order = Order(time=time, symbol=symbol, side=const.OrderType.SHORT, qty=position_qty, price=data['open'])
                orders.append(order)

        return orders


def run_single(df_cds, config, nlargest, fee):
    df_cds_sub = df_cds.copy()

    ##STRATEGY##
    strategy = AtrStrategy()

    # get_indicators
    indicators = {'atr_mas' : [2,3,5,7,14,21,25,80,99]}
    df_cds_sub = strategy.get_indicators(df_cds_sub, indicators)

    #2list
    data_list = util.df2list_by_date_group(df_cds_sub)


    strategy.config = config

    ##ACCOUNT##
    INITIAL_BALANCE = 1e3
    initial_time = df_cds_sub.iloc[0]['time']
    account = Account(initial_time, INITIAL_BALANCE)

    ##RUN BROKER##
    broker = Broker(account, data_list, strategy, fee)
    broker.run()
    final_balance = broker.account.wallet_balance
    print('final_balance:\n{}\n'.format(final_balance))


    pctcum = pct(INITIAL_BALANCE, final_balance)
    print('final_balance pctcum :\n{}\n'.format(pctcum))

    #result
    result, _, _ = broker.get_result_stats()
    return result

def run_opt(df_cds, fee, strategy_name, max_hold_symbols):
    ##
    df_cds_sub = df_cds.copy()
    print('len(df_cds_sub):\n{}\n'.format(len(df_cds_sub)))

    ##STRATEGY##
    strategy = AtrStrategy()

    # get_indicators
    indicators = {'atr_mas' : [2,3,5,7,14,21,25,30,40,50,60,80,99]}
    df_cds_sub = strategy.get_indicators(df_cds_sub, indicators)

    #2list
    data_list = util.df2list_by_date_group(df_cds_sub)

    ##OPTIMIZER##
    optimizer = Optimizer()

    #params
    params = {}

    #small range
    params['atr_ma'] = [2,3,5,7,14,21,25,30,40,50,60,80,99]
    params['ma'] = [2,3,5,7,14,21,25,30,40,50,60,80,99]
    params['max_hold_symbols'] = [max_hold_symbols]

    params['pct_sl'] = np.arange(-0.03, 0, 0.005).tolist()
    params['pct_tp'] = np.arange(0, 0.03, 0.005).tolist()
    params['lev'] = [10,5,3]

    optimizer.shuffle_params(params)
    param_batch = optimizer.get_cartesian_params(params)

    #run
    results = optimizer.optimize(data_list, strategy, param_batch, fee, strategy_name)
    print('len(opt_results):\n{}\n'.format(len(results)))
    df = pd.DataFrame(results)
    print(df.sort_values('pctcum')[-10:][['pctcum','sr','max_drawdown','num_trades']])


if __name__ == '__main__':

    #FEE
    # LIMIT_FEE = 0.036 / 100
    LIMIT_FEE = 0
    fee = LIMIT_FEE

    #exchange
    exchange = 'binance_future'

    #symbol
    symbols = ['ETH/USDT', 'BTC/USDT']
    symbols = ['ETH/USDT']
    # symbols = ['BTC/USDT']

    #frame
    frame = '1d'
    # frame = '1h'


    ##READ##
    df_cds = read(exchange, frame, symbols)
    print('df_cds range:{}~{}'.format(df_cds.iloc[0]['time'], df_cds.iloc[-1]['time']))
    df_cds = df_cds[df_cds['time'] > '2021-01-01']
    
    #tops by volume 1d
    nlargest = len(symbols)
    max_hold_symbols = nlargest
    ma = 5
    df_cds_tops = get_tops_by_vol(df_cds, ma, nlargest)
    print('df_cds_tops range:{}~{}'.format(df_cds_tops.iloc[0]['time'], df_cds_tops.iloc[-1]['time']))

    config = {
        'lev':5,
        'max_hold_symbols': max_hold_symbols,

        'atr_ma':14,
        'ma':14,
        'pct_tp' : 1/100, 'pct_sl' : -1/100}

    result = run_single(df_cds_tops, config, nlargest, fee)
    print('run_single result:\n{}\n'.format(result))

    ##RUN OPT##
    strategy_name = 'atr'
    run_opt(df_cds_tops, fee, strategy_name, max_hold_symbols)
