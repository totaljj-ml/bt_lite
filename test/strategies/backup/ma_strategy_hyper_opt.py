from hyperopt import fmin, tpe, space_eval
from hyperopt import hp

from bt_lite.core import Order, Broker, Account, Optimizer
from test.test_util import read

import pandas as pd
import numpy as np

from bt_lite.common import const, util
from bt_lite.common.util import pct
from bt_lite.indicators import moving_average, rsi
from bt_lite.strategy import Strategy


def run_opt(df_cds, fee, strategy_name, max_hold_symbols):
    ##
    df_cds_sub = df_cds.copy()
    print('len(df_cds_sub):\n{}\n'.format(len(df_cds_sub)))

    ##STRATEGY##
    strategy = MaStrategy()

    # get_indicators
    indicators = { 'emas': [2,3,5,7,14,21,25,99], 'rsi_periods': [7, 14] }
    df_cds_sub = strategy.get_indicators(df_cds_sub, indicators)

    #2list
    data_list = util.df2list_by_date_group(df_cds_sub)

    #large range
    emas = [2, 3, 5, 7, 14, 99]
    for ema in emas:

        lev = 4
        num_splits = 1

        #
        min_ = df_cds_sub['pct_ema{}_close_s1'.format(ema)].min()
        max_ = df_cds_sub['pct_ema{}_close_s1'.format(ema)].max()

        #
        open_low = hp.uniform('open_low', min_, max_)
        open_high = hp.uniform('open_high', min_, max_)

        close_low = hp.uniform('close_low', min_, max_)
        close_high = hp.uniform('close_high', min_, max_)

        pct_sl = hp.uniform('pct_sl', -0.02, 0)
        pct_tp = hp.uniform('pct_tp', 0, 0.02)

        # define a search space
        print('opt')
        space = hp.choice('a',
                          [
                      (strategy, data_list, fee, lev, num_splits, ema, open_low, open_high, close_low, close_high, pct_sl, pct_tp)
                          ])

        space = {
            'strategy' : strategy, 'data_list' : data_list, 'fee_': fee, 'lev' : lev, 'num_splits' : num_splits, 'ema' : ema, 'open_low' : open_low, 'open_high' : open_high, 'close_low' : close_low, 'close_high' : close_high, 'pct_sl' : pct_sl, 'pct_tp' : pct_tp
            # 'x': hp.uniform('x', -6, 6),
            # 'y': hp.uniform('y', -6, 6)
        }

        print('opt2')
        best = fmin(run_single, space, algo=tpe.suggest, max_evals=100)
        print(best)


class MaStrategy(Strategy):

    def __init__(self):
        super().__init__()
        self.config = None
        self.num_long = 0

    def get_indicators(self, data, indicators):
        df_cds = data


        #shift ohlc
        cols_shift = ['open', 'high', 'low', 'close']
        for col_shift in cols_shift:
            util.shift(df_cds, col_shift, 1)

        ##ma##
        emas = indicators['emas']
        sfts = [1]

        moving_average.set_ema(df_cds, emas, sfts)
        moving_average.set_pct_ema_close(df_cds, emas, sfts)

        ##rsi#
        #todo
        rsi_periods = indicators['rsi_periods']
        for rsi_period in rsi_periods:
            rsi.set_rsi(df_cds, rsi_period)

        return df_cds

    def execute(self, state):

        #orders
        orders = []

        #1d
        # df_cds1d = self.df_cds1d

        #account
        account = state['account']

        #data
        data = state['data']
        time = data['time']
        symbol = data['symbol']

        #balance
        wallet_balance = account.wallet_balance
        margin_balance_low = account.margin_balance_low
        if(wallet_balance < 0): return orders
        if(margin_balance_low < 0):
            # print('wallet_balance_low < 0')
            return orders

        


        #position
        position = account.positions[symbol]
        position_side = position.side
        position_qty = position.qty
        pct_unpnl = position.unrealized_pnl

        #leverage
        lev = self.config['lev']

        #num_splits
        num_splits = self.config['num_splits']

        ##이격도##
        criteria_col = 'pct_ema{}_close_s1'.format(self.config['ema'])
        criteria = data[criteria_col]
        if (position_side == const.Position.NONE):
            # OPEN LONG
            if (
                    (data['open'] > data['ema99_s1']) &
                    (criteria > self.config['open_low']) &
                    (criteria < self.config['open_high'])
            ):
                qty = wallet_balance * lev / num_splits / max_hold_symbols / data['open']

                # todo open or close shift
                order = Order(time=time, symbol=symbol, side=const.OrderType.LONG, qty=qty, price=data['open'])
                orders.append(order)
                self.num_long += 1


        elif (position_side == const.Position.LONG):
            # CLOSE LONG(CONDITION)
            # if (
            #         (criteria > self.config['close_low']) &
            #         (criteria < self.config['close_high'])
            # ):
            #     order = Order(time=time, symbol=symbol, side=const.OrderType.SHORT, qty=position_qty, price=data['open'])
            #     orders.append(order)
            #     self.num_long = 0

            #CLOSE_LONG(TAKE PROFIT)
            if (
                    pct_unpnl > self.config['pct_tp']
            ):
                order = Order(time=time, symbol=symbol, side=const.OrderType.SHORT, qty=position_qty, price=data['open'])
                orders.append(order)
                self.num_long = 0

            #OPEN SPLIT
            elif (
                    self.num_long == 1 &
                    pct_unpnl > self.config['long1_pnl_pct']
            ):
                order = Order(time=time, symbol=symbol, side=const.OrderType.SHORT, qty=position_qty, price=data['open'])
                orders.append(order)
                self.num_long += 1
            elif (
                    self.num_long == 2 &
                    pct_unpnl > self.config['long2_pnl_pct']
            ):
                order = Order(time=time, symbol=symbol, side=const.OrderType.SHORT, qty=position_qty, price=data['open'])
                orders.append(order)
                self.num_long += 1


        return orders

def run_single(args):
    # strategy, data_list, fee_, lev, num_splits, ema, open_low, open_high, close_low, close_high, pct_sl, pct_tp = args
    strategy, data_list, fee_, lev, num_splits, ema, open_low, open_high, close_low, close_high, pct_sl, pct_tp = \
        args['strategy'], args['data_list'], args['fee_'], args['lev'], args['num_splits'], args['ema'], args['open_low'], args['open_high'], args['close_low'], args['close_high'], args['pct_sl'], args['pct_tp']
    print(lev, num_splits, ema, open_low, open_high, close_low, close_high, pct_sl, pct_tp)

    # df_cds_sub = df_cds.copy()
    #
    # ##STRATEGY##
    # strategy = MaStrategy()
    #
    # # get_indicators
    # indicators = { 'emas': [2,3,5,7,14,21,25,99], 'rsi_periods':[7,14] }
    # df_cds_sub = strategy.get_indicators(df_cds_sub, indicators)
    # data_list = util.df2list(df_cds_sub)
    #
    # #config
    config = { 'lev': lev, 'num_splits': num_splits, 'ema': ema, 'open_low': open_low, 'open_high': open_high, 'close_low': close_low, 'close_high': close_high, 'pct_sl': pct_sl, 'pct_tp': pct_tp, }
    strategy.config = config

    ##ACCOUNT##
    INITIAL_BALANCE = 1e3
    initial_time = df_cds_sub.iloc[0]['time']
account = Account(initial_time, INITIAL_BALANCE)
##RUN BROKER##
    broker = Broker(account, data_list, strategy, fee_)
    broker.run()
    final_balance = broker.account.wallet_balance
    print('final_balance:\n{}\n'.format(final_balance))

    #result
    result, _, _ = broker.get_result_stats()
    return result['pctcum']

if __name__ == '__main__':

    #FEE
    LIMIT_FEE = 0.036 / 100
    fee = LIMIT_FEE

    #FRAMES

    frame = '1d'
    frame = '1h'
    symbols = ['ETH/USDT', 'BTC/USDT']
    exchange = 'binance_future'

    ##READ##
    df_cds = read(exchange, frame, symbols)
    # df_cds = df_cds[df_cds['time'] > '2022-03-01']
    print('df_cds range:{}~{}'.format(df_cds.iloc[0]['time'], df_cds.iloc[-1]['time']))


    ##RUN SINGLE##
    # lev = 3
    # num_splits = 1
    # ema = 3
    # open_low = 0.0
    # open_high = 0.01
    # close_low = -0.02
    # close_high = -0.02
    # pct_sl = -0.01
    # pct_tp = 0.01

    # result = run_single(df_cds, fee, lev, num_splits, ema, open_low, open_high, close_low, close_high, pct_sl, pct_tp)
    # print('run_single result:\n{}\n'.format(result))


    ##RUN OPT##
    strategy_name = 'ma'
    run_opt(df_cds, fee, strategy_name, max_hold_symbols)




    #atr 이 잘 나왔음.. okshin, chartist 보기


