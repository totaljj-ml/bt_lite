def SMA(data, period=30, column='Close'):
    return data[column].rolling(window=period).mean()


def EMA(data, period=20, column='Close'):
    #     return data[column].ewm(span=period, adjust=False).mean()
    #     return data[column].ewm(span=period).mean()
    return data[column].ewm(com=period - 1, min_periods=period).mean()


def _get_rsi(data, period=14, column='close'):
    #     print('data:\n{}'.format(data[:2]))
    # diff
    delta = data[column].diff(
        1)  # Use diff() function to find the discrete difference over the column axis with period value equal to 1
    delta = delta.dropna()  # or delta[1:]

    # updown copy
    up = delta.copy()  # Make a copy of this object’s indices and data
    down = delta.copy()  # Make a copy of this object’s indices and data
    data['delta'] = delta

    # updown
    up[up < 0] = 0
    down[down > 0] = 0
    data['up'] = up
    data['down'] = down

    #     print('data:\n{}\n'.format(data[['close','up','down','delta']]))
    #     AVG_Gain = SMA(data, period, column='up')#up.rolling(window=period).mean()
    #     AVG_Loss = abs(SMA(data, period, column='down'))#abs(down.rolling(window=period).mean())
    AVG_Gain = EMA(data, period, column='up')  # up.rolling(window=period).mean()
    AVG_Loss = abs(EMA(data, period, column='down'))  # abs(down.rolling(window=period).mean())

    #     print('AVG_Gain:\n{}'.format(AVG_Gain))
    #     print('AVG_LOSS:\n{}'.format(AVG_Loss))

    RS = AVG_Gain / AVG_Loss
    #     print('RS:\n{}'.format(RS))

    RSI = 100.0 - (100.0 / (1.0 + RS))
    #     print('RSI:\n{}'.format(RSI))

    return RSI


def set_rsi(df_cds, period):
    #todo 아래 실행시 copy가 되어 버림(set 안됨)
    #return 으로 다 바꿔야함
    # df_cds = df_cds.sort_values(['symbol', 'time']).reset_index(drop=True)

    col = 'rsi{}'.format(period)
    df_cds[col] = df_cds.groupby(['symbol']).apply(_get_rsi, period).reset_index(drop=True).T

    df_cds['{}_s1'.format(col)] = df_cds.groupby(['symbol'])[col].shift(1)

#     df_rsi
#     df_rsi = df_rsi.rename(columns={0: 'rsi'}).drop(columns='symbol')
#     print(df_cds['rsi'])

#     # pd.merge(df_cd, df_rsi.set_index('level_1'), left_index=True, right_on='level_1')
#     df_cds = pd.merge(df_cds, df_rsi.set_index('level_1'), left_index=True, right_index=True)
#     return df_cds

