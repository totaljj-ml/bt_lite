import io

import quantstats as qs

from bt_lite.core import Account, Broker
from bt_lite.indicators import moving_average, tops
from test.strategies.ma.ma_strategy_grid_search import MaStrategy
from test.strategies.vola.vola1d_strategy_two import VolaStrTwo
from test.test_util import read

qs.extend_pandas()

import pandas as pd

from bt_lite.common import util, const, graph

#FEE
LIMIT_FEE = 0.036 / 100

###READ####
##STRATEGY##

symbols = ['ETH/USDT', 'LUNA/USDT']
df_cds = read('binance_future', '1d', symbols)
# df_cds = df_cds[df_cds['time'] > '2021-07-01']
# df_cds = df_cds[df_cds['time'] > '2021-11-01']
df_cds = df_cds[df_cds['time'] >= '2022-02-01']
df_cds = df_cds[df_cds['time'] < '2022-03-24']

df_cds = df_cds.reset_index(drop=True)

#set nlargest, max_hold_symbols
nlargest = len(symbols)
max_hold_symbols = nlargest

#tops by volume 1d
ma = 1
moving_average.set_volume_ma(df_cds, ma)
df_cds_tops = tops.get_nlargest(df_cds, 'vol_ma{}_s1'.format(ma), nlargest)
df_cds_tops = df_cds_tops.reset_index(drop=True)
print('df_cds_tops range:{}~{}'.format(df_cds_tops.iloc[0]['time'], df_cds_tops.iloc[-1]['time']))

# repeat to loop the bar two times(at open and close)
N = 2
df_cds_tops = (df_cds_tops
               .groupby(['time'], group_keys=False)
               .apply(lambda d: pd.concat([d.assign(is_open_bar=True if n == 0 else False) for n in range(N)]))
               )

#MAKE STRATEGY
strategy = VolaStrTwo()

# get_indicators
indicators = {'emas': [2, 3, 5, 7, 14, 21, 25, 99], 'rsi_periods': [7, 14], }
df_cds_sub = strategy.get_indicators(df_cds_tops, indicators)

#2list
data_list = util.df2list_by_date_group(df_cds_sub)


##GET OPTIMIZED CONFIG##
#read
df_results = pd.read_csv('{}/data/optimizer_results/2022_04_08__08_01_08_vola_1/results.csv'.format(const.ROOT_DIR))

#best result
df_results = df_results.sort_values('pctcum')
config = df_results.iloc[-1].to_dict()
trades_csv = io.StringIO(config['trades'])
df_trades = pd.read_csv(trades_csv, sep=',')
print(df_trades)

#set integer
# config['ema'] = int(config['ema'])

#set config to strategy
strategy.config = config
print("strategy.config:{}".format(strategy.config))

##ACCOUNT##
INITIAL_BALANCE = 1e3
initial_time = df_cds_sub.iloc[0]['time']
account = Account(initial_time, INITIAL_BALANCE)

##RUN BROKER##
broker = Broker(account, data_list, strategy, LIMIT_FEE)
broker.run()
final_balance = broker.account.wallet_balance
print('final_balance : {}'.format(final_balance))

#
# print(pctcum * 100)

result, _, _ = broker.get_result_stats()
print('run_single result:\n{}\n'.format(result))


##DRAW##
df_trades = broker.get_trades()
print('df_trades:\n{}\n'.format(df_trades))
# df_cds = df_cds[df_cds['time'] > '2022-01-01']
# df_trades = df_trades[df_trades['time'] > '2022-01-01']
graph.draw(df_cds, df_trades)

#공유하기

