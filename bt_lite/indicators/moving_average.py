from bt_lite.common import util


# fixme index reset_index 꼭해야함
# fixme index reset_index 꼭해야함
# fixme index reset_index 꼭해야함

def set_ma(df, column_src, ma):
    column_ma = 'ma{}'.format(ma, column_src)
    df[column_ma] = df.groupby('symbol')[column_src].apply(lambda x:x.ewm(ma).mean())


def set_ema(df, emas, sfts):
    for ema in emas:
        column_ema = 'ema{}'.format(ema)
        # df[column_ema] = df.groupby('symbol')[column_src].apply(lambda x:x.ewm(ema).mean())

        #only close
        df[column_ema] = df.groupby('symbol')['close'].apply(lambda x : x.ewm(span=ema).mean())
        for sft in sfts:
            util.shift(df, column_ema, sft)

def set_pct_ema_close(df, emas, sfts):
    for ema in emas:
        col = 'pct_ema{}_close'.format(ema)
        df[col] = util.pct(df['ema{}'.format(ema)], df['close'])
        for sft in sfts:
            util.shift(df, col, sft)

# def pct_ema_close(df, emas, sfts):
#     for ema in emas:
#         df[col] = df.groupby('symbol')[col].apply(lambda x:x.ewm(ema).mean())
#         for sft in sfts:
#             util.shift(df, col, sft)
# 

def set_volume_ma(df_cds, vol_ma):
    #fixme index reset_index 꼭해야함

    col = 'vol_ma{}'.format(vol_ma)
    df_cds[col] = df_cds.groupby('symbol')['quote_vol'].rolling(vol_ma).mean().reset_index(drop=True)
    df_cds['{}_s1'.format(col)] = df_cds.groupby('symbol')[col].shift()
