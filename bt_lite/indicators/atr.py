from bt_lite.common import util


def set_atr(df, ma):
    df['tr'] = [max(tup) for tup in list(zip(df['high'] - df['low'],
                                             (df['high'] - df['close_s1']).abs(),
                                             (df['low'] - df['close_s1']).abs()))]

    col = 'atr{}'.format(ma)
    df[col] = df.groupby('symbol')['tr'].rolling(ma).mean().reset_index(drop=True)
