import random
import string

import plotly.graph_objects as go
import yaml
from plotly.subplots import make_subplots

from bt_lite.common import const


def draw(df_cds, df_trades):
    fig = make_subplots(rows=1, cols=1,
                        specs=[
                            [{}],
                        ],
                        shared_xaxes=True,
                        subplot_titles=("First Subplot", "Second Subplot"))

    fig1 = go.Candlestick(x=df_cds['time'], open=df_cds['open'], high=df_cds['high'], low=df_cds['low'], close=df_cds['close'])

    fig.add_trace(fig1, row=1, col=1)

    fig.update_layout(showlegend=False, title_text="Specs with Subplot Title", xaxis_rangeslider_visible=False)

    for _, df_trade in df_trades.iterrows():
        time_ = df_trade['time']
        side = df_trade['side']
        price = df_trade['price']
        if (side == 1):
            fig.add_annotation(x=time_, y=price, text="long", showarrow=True, yshift=0, font=dict( family="Courier New, monospace", size=16, color="#2ca02c" ), arrowhead=2, arrowsize=1, arrowwidth=2, arrowcolor="#2ca02c", )
        elif (side == -1):
            fig.add_annotation(x=time_, y=price, text="short", showarrow=True, yshift=0, font=dict( family="Courier New, monospace", size=16, color="#d62728" ), arrowhead=2, arrowsize=1, arrowwidth=2, arrowcolor="#d62728", )
    fig.show()





