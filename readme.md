##pip##
pip3 install mysqlclient
pip3 install pandas
pip3 install numpy
pip3 install plotly
pip3 install hyperopt

##DB##
CREATE DATABASE bt_lite;
create user 'bt_lite'@'%' identified by 'bt_lite1';
ALTER USER 'bt_lite'@'%' IDENTIFIED BY 'bt_lite1';
grant all privileges on bt_lite.* to bt_lite@'%' with grant option;
GRANT PROCESS ON *.* TO bt_lite@'%';
flush privileges;


##conf##
rename conf.yaml.bak to conf.yaml
modify conf.yaml