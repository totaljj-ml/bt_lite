
#symbol
import pandas as pd
from tqdm import tqdm

from bt_lite.common import util, const
from bt_lite.common.util import divide_chunks, pct
from test.test_util import read, get_tops_by_vol

pd.set_option('display.max_columns', None)
# pd.set_option('display.max_rows', None)

df = pd.read_csv('{}/test.csv'.format(const.ROOT_DIR))
df['open_vol'] = df['open_qty'] * df['open_price']
df['vol'] = df['price'] * df['qty']

df['open+pnl'] = df['wallet_balance_at_open'] + df['realized_pnl']

# df['open_vol'] = df['open_qty'] * df['open_price']
# print(df)

# df= df[df['time'] >= '2021-06-01']
# df = df[df['time'] >= '2021-05-31']

cols = ['time','side',
        'open_vol','vol',
        'margin_balance','wallet_balance',
        'open+pnl',
        'wallet_balance_at_open', 'realized_pnl',
        # 'open_price','price',

        # 'pct','pct_margin','pct_wallet'
        ]
#todo 12 번 test.py
# if (int(vol) == 1065): vola1d_strategy.py
#         # todo
#         # todo
#         # todo
#         # todo
#         # todo
#
# #todo
# #unpnl 두번 하기 때문인듯. 밖으로 빼던지 해야할드 . core.py
# #trades = self.execute_unfilled_orders(unpnl)
# # undo unpnl
# self.account.margin_balance -= unpnl
# # add realized_pnl
# self.account.margin_balance += realized_pnl


print(df[cols][10:15])
print('------------')
# d = df[round(df['pct'], 6) != round(df['pct_margin'], 6)]
# print(d[cols][:5])
