
from bt_lite.core import Order, Broker, Account, Optimizer
from test.test_util import read

import pandas as pd
import numpy as np

from bt_lite.common import const, util, graph
from bt_lite.common.util import pct, divide_chunks
from bt_lite.indicators import moving_average, rsi, tops
from bt_lite.strategy import Strategy

def run_opt(df_cds, fee, strategy_name, max_hold_symbols):
    ##
    df_cds_sub = df_cds.copy()
    print('len(df_cds_sub):\n{}\n'.format(len(df_cds_sub)))

    ##STRATEGY##
    strategy = MaStrategy()

    # get_indicators
    indicators = { 'emas': [2,3,5,7,14,21,25,50,99], 'rsi_periods': [7, 14] }
    df_cds_sub = strategy.get_indicators(df_cds_sub, indicators)

    #2list
    data_list = util.df2list_by_date_group(df_cds_sub)

    ##OPTIMIZER##
    optimizer = Optimizer()

    #params
    params = {}

    #large range
    emas = [2, 3, 5, 7, 14, 99]
    # emas = [2, 3, 5, 7, 14, 99]
    # emas = [25, 7 ,99]
    emas = [50]
    num_buckets = 5
    for ema in emas:
        print('-------------')
        print('ema:\n{}\n'.format(ema))
        params['ema'] = [ema]
        params['lev'] = [2,3,4]
        params['lev'] = [10]
        params['num_splits'] = [2]

        # todo run_single 등으로 graph 확인
        #2701 백 찾았음 lev 10배?(2022_03_26__18_59_01_ma_2701 ,src 확인)

        #
        min_ = df_cds_sub['pct_ema{}_close_s1'.format(ema)].min()
        max_ = df_cds_sub['pct_ema{}_close_s1'.format(ema)].max()
        q1 = df_cds_sub['pct_ema{}_close_s1'.format(ema)].quantile(0.1)
        q2 = df_cds_sub['pct_ema{}_close_s1'.format(ema)].quantile(0.2)
        q3 = df_cds_sub['pct_ema{}_close_s1'.format(ema)].quantile(0.3)
        q4 = df_cds_sub['pct_ema{}_close_s1'.format(ema)].quantile(0.4)
        q5 = df_cds_sub['pct_ema{}_close_s1'.format(ema)].quantile(0.5)
        q6 = df_cds_sub['pct_ema{}_close_s1'.format(ema)].quantile(0.6)
        q7 = df_cds_sub['pct_ema{}_close_s1'.format(ema)].quantile(0.7)
        q8 = df_cds_sub['pct_ema{}_close_s1'.format(ema)].quantile(0.8)
        q9 = df_cds_sub['pct_ema{}_close_s1'.format(ema)].quantile(0.9)


        #
        size = (q6 - 0)/num_buckets
        print('q6:\n{}\n'.format(q6))
        params['open_low'] = np.arange(0, q6, size).tolist()
        params['open_high'] = np.arange(0, q6, size).tolist()

        size = (0 - q4)/num_buckets
        print('q4:\n{}\n'.format(q4))
        params['close_low'] = np.arange(q4, 0, size).tolist()
        params['close_high'] = np.arange(q4, 0, size).tolist()

        params['long1_pnl_pct'] = np.arange(-20/100, -10/100, 5/100).tolist()
        params['long2_pnl_pct'] = [0]

        params['pct_sl'] = [-1/100]
        params['pct_tp'] = np.arange(0, 10/100, 2/100).tolist()

        optimizer.shuffle_params(params)
        param_batch = optimizer.get_cartesian_params(params)

        #run
        results = optimizer.optimize(data_list, strategy, param_batch, fee, strategy_name)
        df = pd.DataFrame(results)
        print(df.sort_values('pctcum')[-10:][['pctcum','sr','max_drawdown','num_trades']])

class MaStrategy(Strategy):

    def __init__(self):
        super().__init__()
        self.config = None
        self.num_long = 0

    def get_indicators(self, data, indicators):
        df_cds = data

        #shift ohlc
        cols_shift = ['open', 'high', 'low', 'close']
        for col_shift in cols_shift:
            util.shift(df_cds, col_shift, 1)

        ##ma##
        emas = indicators['emas']
        sfts = [1]

        moving_average.set_ema(df_cds, emas, sfts)
        moving_average.set_pct_ema_close(df_cds, emas, sfts)

        ##rsi#
        #todo
        rsi_periods = indicators['rsi_periods']
        for rsi_period in rsi_periods:
            rsi.set_rsi(df_cds, rsi_period)

        return df_cds

    def execute(self, state):

        #orders
        orders = []

        #1d
        # df_cds1d = self.df_cds1d

        #account
        account = state['account']

        #data
        data = state['data']
        time = data['time']
        symbol = data['symbol']

        #balance
        wallet_balance = account.wallet_balance
        margin_balance = account.margin_balance
        margin_balance_low = account.margin_balance_low
        if(wallet_balance < 0): return orders

        #liquidiated
        if(margin_balance_low  < 0):
            # print('wallet_balance_low < 0')
            return orders

        
        

        #position
        position = account.positions[symbol]
        position_side = position.side
        position_qty = position.qty
        pct_unpnl = position.unrealized_pnl

        #leverage
        lev = self.config['lev']

        #num_splits
        num_splits = self.config['num_splits']

        #max hold symbols
        max_hold_symbols = self.config['max_hold_symbols']

        ##이격도##
        criteria_col = 'pct_ema{}_close_s1'.format(self.config['ema'])
        criteria = data[criteria_col]
        if (position_side == const.Position.NONE):
            # OPEN LONG
            if (
                    (data['open'] > data['ema99_s1']) &
                    (criteria > self.config['open_low']) &
                    (criteria < self.config['open_high'])
            ):
                qty = wallet_balance * lev / num_splits / max_hold_symbols / data['open']

                # todo open or close shift
                order = Order(time=time, symbol=symbol, side=const.OrderType.LONG, qty=qty, price=data['open'])
                orders.append(order)
                self.num_long += 1


        elif (position_side == const.Position.LONG):
            # CLOSE LONG(CONDITION)
            if (
                    (criteria > self.config['close_low']) &
                    (criteria < self.config['close_high'])
            ):
                order = Order(time=time, symbol=symbol, side=const.OrderType.SHORT, qty=position_qty, price=data['open'])
                orders.append(order)
                self.num_long = 0

            #CLOSE_LONG(TAKE PROFIT)
            elif (
                    pct_unpnl > self.config['pct_tp']
            ):
                order = Order(time=time, symbol=symbol, side=const.OrderType.SHORT, qty=position_qty, price=data['open'])
                orders.append(order)
                self.num_long = 0

            #OPEN SPLIT
            elif (
                    (num_splits > 1) &
                    (self.num_long == 1) &
 (pct_unpnl > self.config['long1_pnl_pct'])
            ):
                qty = wallet_balance_at_open * lev / num_splits / max_hold_symbols / data['open']
                order = Order(time=time, symbol=symbol, side=const.OrderType.LONG, qty=position_qty, price=data['open'])
                orders.append(order)
                self.num_long += 1
            # elif (
            #         (self.num_long == 2) &
            #         (pct_unpnl > self.config['long2_pnl_pct'])
            # ):
            #     order = Order(time=time, symbol=symbol, side=const.OrderType.SHORT, qty=position_qty, price=data['open'])
            #     orders.append(order)
            #     self.num_long += 1


        return orders

def run_single(df_cds, config, nlargest, fee):
    df_cds_sub = df_cds.copy()

    ##STRATEGY##
    strategy = MaStrategy()

    # get_indicators
    indicators = { 'emas': [2,3,5,7,14,21,25,99], 'rsi_periods':[7,14] }
    df_cds_sub = strategy.get_indicators(df_cds_sub, indicators)

    #2list
    data_list = util.df2list_by_date_group(df_cds_sub)

    #config
    strategy.config = config

    ##ACCOUNT##
    INITIAL_BALANCE = 1e3
    initial_time = df_cds_sub.iloc[0]['time']
account = Account(initial_time, INITIAL_BALANCE)
##RUN BROKER##
    broker = Broker(account, data_list, strategy, fee)
    broker.run()
    final_balance = broker.account.wallet_balance
    print('final_balance:\n{}\n'.format(final_balance))

    ##draw##
    df_trades = broker.get_trades()
    df_trades.to_csv('{}/test.csv'.format(const.ROOT_DIR), index=False)
    #debug
    df_trades = df_trades[-100:]
    # #graph.draw(df_cds, df_trades)
    print(df_trades[-3:])

    return broker

if __name__ == '__main__':

    #FEE
    LIMIT_FEE = 0.036 / 100
    fee = LIMIT_FEE

    #FRAMES
    frame = '1d'
    frame = '1d'
    symbols = ['ETH/USDT', 'BTC/USDT']
    symbols = ['ETH/USDT']
    nlargest = 1

    exchange = 'binance_spot'
    exchange = 'binance_future'

    ##READ##
    df_cds = read(exchange, frame, symbols)
    # df_cds = df_cds[df_cds['time'] > '2021-01-01']
    # df_cds = df_cds[df_cds['time'] > '2021-05-12']
    # df_cds = df_cds[df_cds['time'] < '2022-03-01']

    #tops by volume 1d
    ma = 5
    moving_average.set_volume_ma(df_cds, ma)

    #todo
    #todo
    #todo
    #todo
    #todo
    #df_cds, df_cds_tops 안 맞음. 1개 symbol 했을떄
    #core_Single 이랑 테스트 해보기

    df_cds_tops = tops.get_nlargest(df_cds, 'vol_ma{}_s1'.format(ma), nlargest)

    print('df_cds range:{}~{}'.format(df_cds.iloc[0]['time'], df_cds.iloc[-1]['time']))
    print('df_cds_tops range:{}~{}'.format(df_cds_tops.iloc[0]['time'], df_cds_tops.iloc[-1]['time']))

    ##RUN SINGLE##
    config = {
         'lev': 1,
         'num_splits': 1,
         'max_hold_symbols': 1,

        #todo
        #todo
        #btc/eth 100배, mdd -10, 3달치 graph 확인, worst margnin low 확인,

        # 'num_splits': 2,
         # 'max_hold_symbols': max_hold_symbols,

        'ema': 3,
         'open_low': 0.0, 'open_high': 0.01,
         'close_low': -0.02, 'close_high': -0.02,
         'long1_pnl_pct': -0.10, 'long2_pnl_pct': -0.10,

         'pct_sl': -0.01, 'pct_tp': 0.01,
     }

    #run
    broker = run_single(df_cds[5:], config, nlargest, fee)
    #result
    result, _, _ = broker.get_result_stats()
    print('run_single result:\n{}\n'.format(result))

    ##RUN OPT##
    # strategy_name = 'ma_grid_search'
    # run_opt(df_cds, fee, strategy_name, max_hold_symbols)


    #atr 이 잘 나왔음.. okshin, chartist 보기

