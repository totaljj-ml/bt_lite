import pandas as pd

from bt_lite.common import util


def get_nlargest(df_cds, column, nlargest):
    #nlargest
    df_tops = df_cds.set_index(['date', 'symbol'])
    # df_tops = df_cds.set_index(['date'])
    df_tops = df_tops.groupby(level=0)[column].nlargest(nlargest)

    #index 정리
    df_tops.index = df_tops.index.droplevel(0)
    df_tops = df_tops.reset_index()
    df_tops = df_tops.drop(columns=[column])

    #timestamp to date
    df_tops['date'] = df_tops['date'].dt.date

    #only tops
    df_tops = pd.merge(df_tops, df_cds, on=['symbol', 'date'])

    #reset_index
    df_tops = df_tops.reset_index(drop=True)
    return df_tops
