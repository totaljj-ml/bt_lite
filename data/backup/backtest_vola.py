
import pandas as pd
import quantstats as qs

from bt_lite.common import const
from bt_lite.common.util import pct
from bt_lite.data.data_stat import get_stat_test

qs.extend_pandas()


class BacktestVola:
    def __init__(self):
        pass

    def run(self, df_tgts, k):
        df_tgts['open_tgt_price'] = df_tgts['vola_s1'] * k + df_tgts['open']
        df_tgts['pct_vola'] = pct(df_tgts['open_tgt_price'], df_tgts['close'])

        df_tgts = df_tgts[df_tgts['high'] > df_tgts['open_tgt_price']]

        df_tgts['pct'] = df_tgts['pct'] - const.BINANCE_FUTURE_FEE*2

        # extend pandas functionality with metrics, etc.
        df_pcts = df_tgts.set_index('time')[['pct']]
        df_pcts = pd.Series(df_pcts['pct'], index=df_pcts.index)

        # qs.reports.full(dft, benchmark=dfb)
        # qs.reports.full(df_pcts)
        result_metrics = qs.reports.metrics(df_pcts, display=False)

        return df_tgts, result_metrics

##
df_s = get_stat_test()

df_tgts = df_s.copy()
df_tgts = df_tgts[df_tgts['close_s1'] > 100]

##
k = 0.4
bt = BacktestVola()
_, result_metrics = bt.run(df_tgts, k)
print(result_metrics)

