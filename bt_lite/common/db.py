# -*- coding: utf-8 -*-

from __future__ import unicode_literals, print_function, division

import traceback

import mysql.connector.pooling
from sqlalchemy import create_engine

from bt_lite.common.const import DB



def get_engine():
    # engine = create_engine("mysql+mysqldb://bt_lite:" + "bt_lite1" + "@localhost/bt_lite", encoding='utf-8')
    engine = create_engine("mysql+mysqldb://{}:{}@{}/{}".format(DB.ID, DB.PWD, DB.HOST, DB.NAME), encoding='utf-8')
    return engine

def getConnection(poolName):
    conn = mysql.connector.connect(user=DB.ID, password=DB.PWD, host=DB.HOST, db=DB.NAME, pool_name = poolName, pool_size = 32)
    return conn

def select(query, params):
    try:
        conn = getConnection('server')

        cursor = conn.cursor()
        cursor.execute(query, params)
        rows = cursor.fetchall()

        #add column name
        num_fields = len(cursor.description)
        col_names = [i[0] for i in cursor.description]
        results = []
        for r in rows:
            temp = {}
            for i, c in enumerate(col_names):
                temp[c] = r[i]
            results.append(temp)

        cursor.close()
        conn.close()

        return results
    except:
        traceback.print_exc()
        return None
    # return rows

def insert(query, params):
    try:
        conn = getConnection('server')
        cursor = conn.cursor()
        cursor.execute(query, params)
        id = cursor.lastrowid
        cursor.close()
        conn.commit()
        conn.close()
        return id
    except:
        traceback.print_exc()
        return None

def execute(query, params):
    try:
        conn = getConnection('server')
        cursor = conn.cursor()
        cursor.execute(query, params)
        cursor.close()
        conn.commit()
        conn.close()
    except:
        traceback.print_exc()


def update(query, params):
    try:
        conn = getConnection('server')
        cursor = conn.cursor()
        cursor.execute(query, params)
        cursor.close()
        conn.commit()
        conn.close()
    except:
        traceback.print_exc()
