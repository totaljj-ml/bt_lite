import numpy as np

from bt_lite.common import util
from test.test_util import read, get_tops_by_vol



def set_ttm_mm(df, length, mult, length_KC, mult_KC):
    def _poly(x, fit_y):
        exp = np.polyfit(fit_y, x, 1)[0] * (length_KC - 1) + np.polyfit(fit_y, x, 1)[1]
        return exp

    ## CALCULATE BB ##
    df['m_avg'] = df.groupby('symbol')['close'].apply(lambda x: x.rolling(window=length).mean())
    df['m_std'] = df.groupby('symbol')['close'].apply(lambda x: x.rolling(window=length).std(ddof=0))
    #     m_avg = df['close'].groupby('symbol').rolling(window=length).mean()
    #     m_std = df['close'].groupby('symbol').rolling(window=length).std(ddof=0)
    #     df['avg'] = m_avg
    #     df['std'] = m_std
    df['upper_BB'] = df['m_avg'] + mult * df['m_std']
    df['lower_BB'] = df['m_avg'] - mult * df['m_std']

    ## CALCULATE TRUE RANGE ##
    df['tr0'] = abs(df["high"] - df["low"])
    df['close_s1'] = df.groupby('symbol')['close'].shift()

    df['tr1'] = abs(df["high"] - df["close_s1"])
    df['tr2'] = abs(df["low"] - df["close_s1"])
    df['tr'] = df[['tr0', 'tr1', 'tr2']].max(axis=1)

    ## CALCULATE KC ##
    df['range_ma'] = df.groupby('symbol')['tr'].apply(lambda x: x.rolling(window=length_KC).mean())
    df['upper_KC'] = df['m_avg'] + df['range_ma'] * mult_KC
    df['lower_KC'] = df['m_avg'] - df['range_ma'] * mult_KC

    ## CALCULATE BAR MM ##
    df['highest'] = df.groupby('symbol')['high'].apply(lambda x: x.rolling(window=length_KC).max())
    df['lowest'] = df.groupby('symbol')['low'].apply(lambda x: x.rolling(window=length_KC).min())
    df['m1'] = (df['highest'] + df['lowest']) / 2
    df['ttm_mm'] = (df['close'] - (df['m1'] + df['m_avg']) / 2)
    fit_y = np.array(range(0, length_KC))

    df = df.reset_index(drop=True)

    #column name
    #eg: ttm_mm_20_1.5_15_1.5
    param_str = '{}_{}_{}_{}'.format(length, mult, length_KC, mult_KC)
    ttm_column_name = 'ttm_mm_{}'.format(param_str)

    #ACTUAL TTM MOMENTUM#
    df[ttm_column_name] = df.groupby('symbol')['ttm_mm'].rolling(window=length_KC).apply(lambda x: _poly(x, fit_y)).reset_index(drop=True)
    #     print(df[['ttm_mm']])
    #     df['ttm_mm'] = df.groupby('symbol')['ttm_mm'].rolling(window = length_KC).apply(lambda x:
    #                               np.polyfit(fit_y, x, 1)[0] * (length_KC-1) +
    #                               np.polyfit(fit_y, x, 1)[1], raw=True).reset_index(drop=True)

    # shift
    df['{}_s1'.format(ttm_column_name)] = df.groupby('symbol')[ttm_column_name].shift(1)
    df['{}_s2'.format(ttm_column_name)] = df.groupby('symbol')[ttm_column_name].shift(2)


    ## CHECK FOR 'SQUEEZE'##
    df['squeeze_on'] = (df['lower_BB'] > df['lower_KC']) & (df['upper_BB'] < df['upper_KC'])
    df['squeeze_off'] = (df['lower_BB'] < df['lower_KC']) & (df['upper_BB'] > df['upper_KC'])

    # shift
    util.shift(df, 'squeeze_on', 1)
    util.shift(df, 'squeeze_on', 2)

    util.shift(df, 'squeeze_off', 1)
    util.shift(df, 'squeeze_off', 2)


    ## BUYING WINDOW FOR LONG POSITION: ##
    # 1. black cross becomes gray (the squeeze is released)
    df['long_cond1'] = (df['squeeze_off_s2'] == False) & (df['squeeze_off_s1'] == True)
    # df['long_cond11'] = (df['squeeze_on_s2'] == True) & (df['squeeze_on_s1'] == False)

    # # 2. bar mm is positive => the bar is light green k
    df['long_cond2'] = df['ttm_mm'] > 0
    df['enter_long'] = df['long_cond1'] & df['long_cond2']
    #

    ## # BUYING WINDOW FOR SHORT POSITION: ##
    # # 1. black cross becomes gray (the squeeze is released)
    df['short_cond1'] = (df['squeeze_off_s2'] == False) & (df['squeeze_off_s1'] == True)

    # # 2. bar mm is negative => the bar is light red
    df['short_cond2'] = df['ttm_mm'] < 0
    df['enter_short'] = df['short_cond1'] & df['short_cond2']


if __name__ == '__main__':


    #FEE
    LIMIT_FEE = 0.036 / 100
    fee = LIMIT_FEE

    #FRAMES
    frame = '1d'
    symbols = ['ETH/USDT', 'BTC/USDT']

    exchange = 'binance_spot'
    exchange = 'binance_future'

    ##READ##
    df_cds = read(exchange, frame, symbols)
    df_cds = df_cds[df_cds['time'] > '2021-01-01']
    df_cds = df_cds.reset_index(drop=True)
    print('df_cds range:{}~{}'.format(df_cds.iloc[0]['time'], df_cds.iloc[-1]['time']))

    #tops by volume 1d
    nlargest = 2
    ma = 5
    df_cds_tops = get_tops_by_vol(df_cds, ma, nlargest)
    print('df_cds_tops range:{}~{}'.format(df_cds_tops.iloc[0]['time'], df_cds_tops.iloc[-1]['time']))

    #
    length = 20
    mult = 2
    length_KC = 20
    mult_KC = 1.5

    set_ttm_mm(df_cds_tops, length, mult, length_KC, mult_KC)