import quantstats as qs

from bt_lite.common.util import divide_chunks
from bt_lite.core import Account, Broker
from test.strategies.atr_strategy import AtrStrategy
from test.test_util import read, get_tops_by_vol

qs.extend_pandas()

import pandas as pd

from bt_lite.common import util, const, graph

#FEE
LIMIT_FEE = 0.036 / 100

exchange = 'binance_future'
symbols = ['ETH/USDT', 'BTC/USDT']

###READ####
frame = '1d'
exchange = 'binance_future'
symbols = ['ETH/USDT', 'BTC/USDT']
# frame = '1h'


##READ##
df_cds = read(exchange, frame, symbols)
print('df_cds range:{}~{}'.format(df_cds.iloc[0]['time'], df_cds.iloc[-1]['time']))
# df_cds = df_cds[df_cds['time'] > '2022-03-01']

# tops by volume
nlargest = 2
ma = 5
df_cds_tops = get_tops_by_vol(df_cds, ma, nlargest)

##STRATEGY##
strategy = AtrStrategy()

# get_indicators
indicators = {'atr_mas': [2, 3, 5, 7, 14, 21, 25, 30, 40, 50, 60, 80, 99]}
df_cds_sub = strategy.get_indicators(df_cds, indicators)

nlargest = 2
# 2list
data_list = util.df2list_by_date_group(df_cds_sub)

##READ RESULTS##
print('read results')
# d = pd.read_csv('{}/data/optimizer_results/2022_03_30__16_40_57_atr_407/results.csv'.format(const.ROOT_DIR))
d = pd.read_csv('test.csv')
print('read results done')

#top pctcum
d = d.sort_values('pctcum')[-1000:]

#config
config = d.iloc[-1].to_dict()
config['atr_ma'] = int(config['atr_ma'])
config['ma'] = int(config['ma'])
strategy.config = config
print("strategy.config:{}".format(strategy.config))

##ACCOUNT##
INITIAL_BALANCE = 1e3
initial_time = df_cds_sub.iloc[0]['time']
account = Account(initial_time, INITIAL_BALANCE)

##RUN BROKER##
broker = Broker(account, data_list, strategy, LIMIT_FEE)
broker.run()
final_balance = broker.account.wallet_balance
print('final_balance : {}'.format(final_balance))

#
# print(pctcum * 100)

lev = 3
result, _, _ = broker.get_result_stats()
print('run_single result:\n{}\n'.format(result))

# draw
df_trades = broker.get_trades()
df_cds = df_cds[df_cds['time'] > '2021-01-01']
df_trades = df_trades[df_trades['time'] > '2021-01-01']
#graph.draw(df_cds, df_trades)

# lev = 10
# fee = LIMIT_FEE
# result = run_single(df_cds_tops, config, nlargest, fee)
# print('run_single result:\n{}\n'.format(result))



