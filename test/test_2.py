
import pandas as pd

df_input = pd.DataFrame( [
        ['01/01', '1', '10'],
        ['01/01', '2', '5'],
        ['01/02', '1', '9'],
        ['01/02', '2', '7'],
], columns=['date','type','value'])

df_output = pd.DataFrame( [
        ['01/01', '1', '10', '1'],
        ['01/01', '2', '5', '1'],
        ['01/01', '1', '10', '2'],
        ['01/01', '2', '5', '2'],

        ['01/02', '1', '9', '1'],
        ['01/02', '2', '7', '1'],
        ['01/02', '1', '9', '2'],
        ['01/02', '2', '7', '2'],
], columns=['date','type','value', 'repeat'])
print(df_output)


N = 2
out = (df_input
      .groupby(['date'], group_keys=False)
      .apply(lambda d: pd.concat([d.assign(repeat=n+1) for n in range(N)]))
      )

print(out)
