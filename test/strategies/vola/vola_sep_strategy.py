import numpy as np
import pandas as pd

from bt_lite.data import data_reader
from bt_lite.common import const, util, graph
from bt_lite.common.util import pct, divide_chunks
from bt_lite.core import Order, Broker, Account, Optimizer
from bt_lite.strategy import Strategy
from test.test_util import read, get_tops_by_vol


#일반 vola랑 값 확인

#leverage by last trade, mdd?
#이격도

class VolaSepStrategy(Strategy):

    def __init__(self):
        super().__init__()
        self.config = None

        self.tgt_price = None
        self.traded_today = False
        self.num_long = 0


    def get_indicators(self, data, indicators):
        ##1d##
        #shift
        data['high_1d_s1'] = data.groupby('symbol')['high_1d'].shift()
        data['low_1d_s1'] = data.groupby('symbol')['low_1d'].shift()
        data['close_1d_s1'] = data.groupby('symbol')['close_1d'].shift()

        return data

    def execute(self, state):
        #orders
        orders = []

        #account
        account = state['account']

        #data
        data = state['data']
        time = data['time']
        symbol = data['symbol']

        #balance
        wallet_balance = account.wallet_balance
        margin_balance_low = account.margin_balance_low
        if(wallet_balance < 0): return orders
        if(margin_balance_low < 0):
            # print('wallet_balance_low < 0')
            return orders
        
        #position
        position = account.positions[symbol]
        position_side = position.side
        position_qty = position.qty
        pct_unpnl = position.unrealized_pnl

        #calculate tgt_price
        k = self.config['k']

        

        if(time.hour == 0):
            # print(df)
            vola = data['high_1d_s1'] - data['low_1d_s1']
            self.tgt_price = vola * k + data['open_1d']

        if(not self.tgt_price) : return orders

        #leverage
        lev = self.config['lev']

        #num_splits
        num_splits = self.config['num_splits']

        #max hold symbols
        max_hold_symbols = self.config['max_hold_symbols']


        if (position_side == const.Position.NONE):
            # LONG
            if (
                    (not self.traded_today) &

                    (self.tgt_price > data['low']) &
                    (self.tgt_price < data['high'])
            ):
                # todo open or close shift

                # tgt long qty
                qty = wallet_balance * lev / num_splits / max_hold_symbols / self.tgt_price

                order = Order(time=time, symbol=symbol, side=const.OrderType.LONG, qty=qty, price=self.tgt_price)
                orders.append(order)

                self.num_long += 1
                self.traded_today = True

                # TIME CUT
                # 23시 open으로 long 들어갈시 23시 close 가격으로 short
                if (time.hour == 23):
                    order = Order(time=time, symbol=symbol, side=const.OrderType.SHORT, qty=qty, price=data['close'])
                    orders.append(order)

                    self.num_long = 0
                    self.traded_today = False
                    self.tgt_price = None

        elif (position_side == const.Position.LONG):
            if (time.hour == 0):
                order = Order(time=time, symbol=symbol, side=const.OrderType.SHORT, qty=position_qty, price=data['open'])
                orders.append(order)

                self.num_long = 0
                self.traded_today = False
                self.tgt_price = None

            #TAKE PROFIT
            elif (pct_unpnl > self.config['pct_tp']):
                order = Order(time=time, symbol=symbol, side=const.OrderType.SHORT, qty=position_qty, price=data['open'])
                orders.append(order)

                self.num_long = 0
                self.traded_today = False
                self.tgt_price = None

            #STOP LOSS
            # elif (pct_unpnl < self.config['pct_sl']):
            #     order = Order(time=time, symbol=symbol, side=const.OrderType.SHORT, qty=position_qty, price=data['open'])
            #     orders.append(order)
            #
            #     self.num_long = 0
            #     self.traded_today = False
            #     self.tgt_price = None

            #SPLIT LONG
            elif (
                    (self.num_long  == 1) &
                    (pct_unpnl < self.config['long1_pnl_pct'])
            ):
                # tgt long qty
                qty = wallet_balance * lev / num_splits / max_hold_symbols / data['open']

                order = Order(time=time, symbol=symbol, side=const.OrderType.LONG, qty=qty, price=data['open'])
                orders.append(order)
                self.num_long += 1

            # # SPLIT LONG
            elif (
                    (self.num_long  == 2) &
                    (pct_unpnl < self.config['long2_pnl_pct'])
            ):
                # tgt long qty
                qty = wallet_balance * lev / num_splits / max_hold_symbols / data['open']

                order = Order(time=time, symbol=symbol, side=const.OrderType.LONG, qty=qty, price=data['open'])
                orders.append(order)
                self.num_long += 1

        return orders

def run_single(df_cds, config, nlargest, fee):
    df_cds_sub = df_cds.copy()

    ##STRATEGY##
    strategy = VolaSepStrategy()

    # get_indicators
    indicators = { }
    df_cds_sub = strategy.get_indicators(df_cds_sub, indicators)

    #2list
    data_list = util.df2list_by_date_group(df_cds_sub)

 
    # todo 청산 ㅠ, timecut?
    # param 조절
    # opt 할때 청산 되면 빼기?

    strategy.config = config

    ##ACCOUNT##
    INITIAL_BALANCE = 1e3
    initial_time = df_cds_sub.iloc[0]['time']
    account = Account(initial_time, INITIAL_BALANCE)

    ##RUN BROKER##
    broker = Broker(account, data_list, strategy, fee)
    broker.run()
    final_balance = broker.account.wallet_balance
    print('final_balance:\n{}\n'.format(final_balance))

    pctcum = pct(INITIAL_BALANCE, final_balance)
    print('final_balance pctcum :\n{}\n'.format(pctcum))

    #draw
    df_trades = broker.get_trades()
    #graph.draw(df_cds, df_trades)

    return broker




def run_opt(df_cds, fee, strategy_name, max_hold_symbols):
    ##
    df_cds_sub = df_cds.copy()
    print('len(df_cds_sub):\n{}\n'.format(len(df_cds_sub)))

    ##STRATEGY##
    strategy = VolaSepStrategy()

    # get_indicators
    indicators = { }
    df_cds_sub = strategy.get_indicators(df_cds_sub, indicators)

    #2list
    data_list = util.df2list_by_date_group(df_cds_sub)

    ##OPTIMIZER##
    optimizer = Optimizer()

    #params
    params = {}

    #large range
    params['lev'] = [7,8]
    params['k'] = [0.4]
    params['num_splits'] = [3]
    params['max_hold_symbols'] = [max_hold_symbols]
    params['long1_pnl_pct'] = np.arange(-40/100, -10/100, 5/100).tolist()
    params['long2_pnl_pct'] = np.arange(-60/100, -20/100, 5/100).tolist()
    params['pct_tp'] = np.arange(0, 50/100, 5/100).tolist()
    # params['pct_sl'] = np.arange(-20/100, -10/100, 5/100).tolist()

    optimizer.shuffle_params(params)
    param_batch = optimizer.get_cartesian_params(params)

    #run
    results = optimizer.optimize(data_list, strategy, param_batch, fee, strategy_name)
    df = pd.DataFrame(results)
    print(df.sort_values('pctcum')[-10:][['pctcum','sr','max_drawdown','num_trades', 'win_rate']])


if __name__ == '__main__':


    #FEE
    LIMIT_FEE = 0.036 / 100
    fee = LIMIT_FEE
    

    #exchange
    exchange = "binance_spot"
    
    #frame 
    frame = '1h'
    #symbols 
    symbols = ['ETH/USDT','BTC/USDT']
    symbols = ['ETH/USDT']

    ##READ##
    #todo 200315 위험한 날 체크 : spot 은 괜찮았음
    # df_cds = df_cds[df_cds['time'] >= '2020-03-15 09:00:00']
    df_cds1d = read(exchange, '1d', symbols)
    df_cds = read(exchange, frame, symbols)
    # df_cds['time'] = df_cds['time'] + datetime.timedelta(hours=9)
    print('df_cds range:{}~{}'.format(df_cds.iloc[0]['time'], df_cds.iloc[-1]['time']))

    #1h, 1d merge : 1h cds  with  1d suffix
    df_cds = pd.merge(df_cds, df_cds1d, on=['symbol','date'], suffixes=['','_1d'])
    # df_cds = df_cds[df_cds['time'] >'2020-04-01']

    #tops by volume 1d
    nlargest = len(symbols)
    max_hold_symbols = nlargest
    ma = 5
    df_cds_tops_1d = get_tops_by_vol(df_cds1d, ma, nlargest)[['symbol','date']]
    # print('df_cds_tops range:{}~{}'.format(df_cds_tops.iloc[0]['time'], df_cds_tops.iloc[-1]['time']))

    #include only top volumes of last date
    df_cds = pd.merge(df_cds, df_cds_tops_1d, on=['symbol','date'], suffixes=['',''])
    df_cds = df_cds.sort_values(['time','symbol'])
    df_cds = df_cds.reset_index(drop=True)
    print('df_cds range:{}~{}'.format(df_cds.iloc[0]['time'], df_cds.iloc[-1]['time']))
    

    ##RUN SINGLE##
    config = {
        'lev': 8, 'k': 0.4,
        'num_splits': 2,
        'max_hold_symbols': max_hold_symbols,

        'long1_pnl_pct': -20 / 100, 'long2_pnl_pct': -30 / 100,
        'pct_tp': 40 / 100, 'pct_sl': -100 / 100
    }

    #run
    broker = run_single(df_cds, config, nlargest, fee)

    #result
    result, _, _ = broker.get_result_stats()
    print('run_single result:\n{}\n'.format(result))

    ##RUN OPT##
    strategy_name = 'vola'
    run_opt(df_cds, fee, strategy_name, max_hold_symbols)

    #split 일때 opti


