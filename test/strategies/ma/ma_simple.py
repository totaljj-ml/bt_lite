from bt_lite.common import graph, const, util
from bt_lite.common.util import pct
import pandas as pd

from bt_lite.indicators import moving_average
from test.test_util import read

pd.set_option('mode.chained_assignment',  None)


symbols = ['ETH/USDT', 'LUNA/USDT']
exchange = "binance_future"

##READ##
df_cds = read(exchange, '1d', symbols)
df_cds = df_cds.sort_values('time')

#liquidiated
# df_cds = df_cds[df_cds['time'] >= '2020-03-12']

##sync vola_simple and vola1d strategy two##
df_cds = df_cds[df_cds['time'] >= '2021-07-11']
df_cds = df_cds[df_cds['time'] < '2022-03-24']
print(df_cds[:2])
print(df_cds[-2:])


##PARAMS##
k = 0.4
lev = 4

#fee
LIMIT_FEE = 0.036 / 100
LIMIT_FEE = 0
fee = LIMIT_FEE

##STRATEGY##

#indicator
##ma##
emas = [2]
sfts = [1]
moving_average.set_ema(df_cds, emas, sfts)
moving_average.set_pct_ema_close(df_cds, emas, sfts)


#trades
# 'ema': 3,
# 'open_low': 0.0, 'open_high': 0.01,
# 'close_low': -0.02, 'close_high': -0.02,
df_tgts = df_cds
df_tgts = df_tgts[df_tgts['pct_ema2_close_s1'] > 0]
df_tgts = df_tgts[df_tgts['pct_ema2_close_s1'] <= 0.01]


df_tgts = df_tgts.sort_values('time')

#pct each symbols
df_tgts['pct'] = (pct(df_tgts['tgt_price'], df_tgts['close']) - fee * 2 ) * lev / len(symbols)
print('df_tgts:\n{}\n'.format(df_tgts[['time','symbol','tgt_price','close','pct']]))

#pct daily
df_multi = df_tgts
df_multi['pct'] = df_multi['pct'].fillna(0)

df_pct = df_multi.groupby('time')['pct'].sum().rename('pct').reset_index()
# df_tgts['pct'] = (pct(df_tgts['tgt_price'], df_tgts['close']) - fee*2 )* lev
df_pct['pct1'] = df_pct['pct'] + 1
df_pct['pctcum'] = df_pct['pct1'].cumprod()
print('df_pct:\n{}\n'.format(df_pct))


#stats
# wins = len(df_tgts[df_tgts['pct'] > 0])
# loses = len(df_tgts[df_tgts['pct'] < 0])
# print(wins/(wins+loses))
#
# #result
# print(df_tgts[['time','tgt_price','close','pctcum','pct']])
#
# #to_csv
# df_tgts.to_csv('{}/test_simple.csv'.format(const.ROOT_DIR), index=False)


##DRAW##
#graph.draw(df_cds, df_trades)


#DRAW MONTHLY
import plotly.express as px
col = 'month'
df = df_tgts
util.set_dates(df)
df['pctcum_m'] = df.groupby(df[col])['pct1'].apply(lambda x:x.cumprod())
df_ = df.groupby(df[col])['pctcum_m'].last() - 1

df_ = df_.reset_index()
fig = px.bar(df_, x=col, y='pctcum_m', width=900, height=600);
# fig.update_yaxes(range=[-0.5, 5])

# fig.show()