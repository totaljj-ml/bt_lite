import datetime
import warnings

import ccxt
import pandas as pd
import pymysql

# from util import db
from bt_lite.common import db, util

DATE_FMT = '%Y-%m-%d'
DATETIME_FMT = '%Y-%m-%d %H:%M:%S'
def to_dt(dt_str, fmt=DATETIME_FMT):
    return datetime.datetime.strptime(dt_str, fmt)
def dt_str(dt, fmt=DATETIME_FMT):
    return datetime.datetime.strftime(dt, fmt)


# MySQL Connector using pymysql
pymysql.install_as_MySQLdb()
warnings.filterwarnings('ignore')


# MySQL Connector using pymysql
pymysql.install_as_MySQLdb()
pd.set_option('use_inf_as_na', True)

config = util.read_config()

binance_key = config['binance_key']
binance_sec = config['binance_sec']
bybit_key = config['bybit_key']
bybit_sec = config['bybit_sec']
huobi_key = config['huobi_key']
huobi_sec = config['huobi_sec']

print('CCXT Version:', ccxt.__version__)

def load_exchange(ex_name, market_name, key, secret, verbose=False):
    exchange = None
    print(ex_name)
    if(ex_name == 'binance'):
        exchange = ccxt.binance({'enableRateLimit': True, 'apiKey': key, 'secret': secret, 'options': {'defaultType': market_name }})
    elif(ex_name == 'bybit'):
        exchange = ccxt.bybit({'enableRateLimit': True, 'apiKey': key, 'secret': secret, 'options': {'defaultType': market_name }})
    elif(ex_name == 'huobi'):
        exchange = ccxt.huobi({'enableRateLimit': True, 'apiKey': key, 'secret': secret, 'options': {'defaultType': market_name }})
    else: raise Exception()

    print('Loading markets from', exchange.id)
    exchange.load_markets()

    exchange.verbose = verbose
    return exchange

verbose = True
verbose = False
EXCHANGES = {}
EXCHANGES['binance_spot'] = load_exchange('binance', 'spot', binance_key, binance_sec, verbose)
EXCHANGES['binance_future'] = load_exchange('binance', 'future', binance_key, binance_sec, verbose)
EXCHANGES['bybit_spot'] = load_exchange('bybit', 'spot', bybit_key, bybit_sec, verbose)
EXCHANGES['bybit_future'] = load_exchange('bybit', 'future', bybit_key, bybit_sec, verbose)
EXCHANGES['bybit_linear'] = load_exchange('bybit', 'linear', bybit_key, bybit_sec, verbose)
EXCHANGES['huobi_spot'] = load_exchange('huobi', 'spot', huobi_key, huobi_sec, verbose)
EXCHANGES['huobi_future'] = load_exchange('huobi', 'future', huobi_key, huobi_sec, verbose)


def get_exchange(exchange_str):
    return EXCHANGES[exchange_str]

def _fetch_ohlcv(exchange_str, symbol, frame, since, limit=100):
    #exchange
    exchange = get_exchange(exchange_str)
    if isinstance(since, str):
        since = exchange.parse8601(since)

    #fetch
    ohlcv = exchange.fetch_ohlcv(symbol, frame, since, limit)

    #df
    df = pd.DataFrame(ohlcv, columns=['time', 'open', 'high', 'low', 'close', 'base_vol'])

    #time
    df['time'] = pd.to_datetime(df['time'] / 1000, unit='s')
    df['date'] = df['time'].dt.date

    #vol
    df['quote_vol'] = df['base_vol'] * df['close']

    return df


def fetch2db(exchange_str, symbol, frame, limit, start_time, end_time):
    date_since = start_time

    while (date_since <= end_time):

        #next date_since
        if(frame.endswith('1d')): date_since += datetime.timedelta(days = limit * 1)
        elif(frame.endswith('1m')): date_since -= datetime.timedelta(minutes = limit)
        elif(frame.endswith('1h')): date_since += datetime.timedelta(hours = limit*1)
        elif(frame.endswith('4h')): date_since += datetime.timedelta(hours = limit*4)
        elif(frame.endswith('12h')): date_since += datetime.timedelta(hours = limit*12)
        else: raise Exception()

        #since_str
        since_str = dt_str(date_since, '%Y-%m-%dT%H:%M:%SZ')

        # fetch
        df_cds = _fetch_ohlcv(exchange_str, symbol, frame, since_str, limit)
        
        # set more columns
        df_cds['symbol'] = symbol
        df_cds['frame'] = frame
        df_cds['exchange'] = exchange_str

        #if no data : conitnue
        if (len(df_cds) == 0):
            print('continue len 0, since_str:\n{}\n'.format(since_str))
            continue
        
        # cut to end_time
        time_newest = df_cds.iloc[-1]['time'].to_pydatetime()
        if (time_newest >= end_time):
            df_cds = df_cds[df_cds['time'] <= end_time]

        # to_sql
        print('inserting : {}~{}'.format(df_cds.iloc[0]['time'], df_cds.iloc[-1]['time']))
        conn = engine.connect()
        df_cds.to_sql(con=conn, name='cds', if_exists='append', index=False)
        conn.close()

        # break if end_time reached
        if (time_newest >= end_time):
            print('break time_newest >= END_DT:\n{}\n'.format(time_newest))
            break


if __name__ == '__main__':

    # create db engine
    engine = db.get_engine()

    # exchange
    exchange_str = 'binance_future'
    exchange_str = 'binance_spot'
    exs = ['binance_future', 'binance_spot']
    for ex in exs:
        exchange = get_exchange(ex)

        # frame
        # frame = '1d'
        #todo everyday update
        #todo everyday update
        #split tables
        frames=['1h','4h','1d']
        # frames=['1h']
        frames=['1d']
        # limit
        limit = 1000

        # time
        start_time = to_dt('2020-01-01', DATE_FMT) - datetime.timedelta(days=limit)  # !!!!!!!!!!!!!!!!!!
        end_time = datetime.datetime.today() - datetime.timedelta(days=1)
        # end_time = to_dt('2022-03-16', DATE_FMT)  # !!!!!!!!!!!!!!!

        ##GET SYMBOLS##
        #tickers
        def get_tickers(exchange):
            tickers = exchange.fetch_tickers()
            symbols = pd.DataFrame(tickers).columns.tolist()
            return symbols

        symbols = get_tickers(exchange)

        #df symbols
        df_symbols = pd.DataFrame(symbols, columns=['symbol'])

        #only USDT
        df_symbols = df_symbols[df_symbols['symbol'].str.endswith('USDT')]

        #tolist
        symbols = df_symbols['symbol'].tolist()
        # symbols = ['BTC/USDT']
        symbols = ['BTC/USDT','ETH/USDT']
        print('len(symbols):\n{}\n'.format(len(symbols)))

        # delete
        # q = 'delete from bt_lite.cds where exchange = "{}" and time >= "2017-1-1" and frame = "{}" '.format(exchange_str, frame)
        # q = 'delete from bt_lite.cds'
        # db.execute(q, (), )

        for frame in frames:
            print('##frame:\n{}\n'.format(frame))
            for symbol in symbols:
                print('###symbol:\n{}\n'.format(symbol))
                fetch2db(exchange_str, symbol, frame, limit, start_time, end_time)
                print('')
            print('')
