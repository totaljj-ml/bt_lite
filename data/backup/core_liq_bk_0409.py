import datetime
import io
import random
import shutil
from itertools import product

import quantstats as qs
from tqdm import tqdm

# minimize the objective over the space

qs.extend_pandas()

import json
import os

import pandas as pd
pd.set_option('mode.chained_assignment',  None)

import numpy as np

from bt_lite.common import const, util
from bt_lite.common.util import pct, generate_random_string

class Logger:
    """
    Logger Object
    """

    def __init__(self):
        self.trade_infos = []
        self.account_infos = []
        # self._unfilled_orders = []

    def to_json(self, target_directory: str) -> None:
        if not os.path.exists(target_directory):
            os.makedirs(target_directory)

        with open(os.path.join(target_directory, "trade_infos.json"), "w") as f:
            f.write(json.dumps(self.trade_infos))

        with open(os.path.join(target_directory, "account_infos.json"), "w") as f:
            f.write(json.dumps(self.account_infos))


class Trade:

    #todo 순서정리
    def __init__(self, symbol, time,
                 price, qty, side,
                 open_price,
                 open_qty,

                 realized_pnl, pct_realized_pnl,
                 pct_realized_pnl_by_price, wallet_balance, margin_balance,

                 wallet_balance_at_open,
                 ):
        self.symbol = symbol
        self.time = time
        self.price = price
        self.qty = qty
        self.side = side


        self.realized_pnl = realized_pnl
        self.pct_realized_pnl = pct_realized_pnl

        #debug
        self.open_price = open_price
        self.open_qty = open_qty

        self.pct_realized_pnl_by_price = pct_realized_pnl_by_price

        self.wallet_balance = wallet_balance
        self.margin_balance = margin_balance


        self.wallet_balance_at_open = wallet_balance_at_open


    def __dict__(self):
        return {
            "symbol": self.symbol,
            "time": self.time,

            "price": self.price,
            "qty": self.qty,
            "side": self.side,

            "open_price": self.open_price,
            "open_qty": self.open_qty,

            "realized_pnl": self.realized_pnl,
            "pct_realized_pnl": self.pct_realized_pnl,
            "pct_realized_pnl_by_price": self.pct_realized_pnl_by_price,

            "wallet_balance": self.wallet_balance,
            "margin_balance": self.margin_balance,

            "wallet_balance_at_open" : self.wallet_balance_at_open,
        }


class Order:
    def __init__(self, time, symbol, side, qty, price):
        self.id = generate_random_string()
        self.time = time
        self.symbol = symbol
        self.side = side
        #         self.type = type
        self.qty = qty
        self.price = price

        self.volume = qty*price


class Position:
    def __init__(self, symbol, side, qty, avg_price, unrealized_pnl):
        self.symbol = symbol
        self.side = side
        self.qty = qty
        self.avg_price = avg_price
        self.unrealized_pnl = unrealized_pnl

        self.wallet_balance_at_open = None

    def get_unrealized_pnl(self, price):
        return (price - self.avg_price) * self.qty * self.side

    # def update_unrealized_pnl(self, price):
    #     self.unrealized_pnl = self.get_unrealized_pnl(price)

    #if doing bar one time, need to implemnt below
    # def get_realized_pnl(self):

    #s1 : qty1 > qty2 -> pnl is subset
    def update(self, price: float, qty: float, side: int) -> float:

        realized_pnl = 0

        if qty == 0:
            return 0.

        #update avg_price
        if (self.side == side) | (self.side == 0):
            self.avg_price = (qty * price + self.qty * self.avg_price) / (
                    qty + self.qty
            )

        #update pnl
        else:
            if self.qty <= qty:
                realized_pnl = ( (price - self.avg_price) * self.side * self.qty )
            else:
                realized_pnl = (price - self.avg_price) * self.side * qty

        new_qty = qty * side + self.qty * self.side

        self.qty, self.side = np.abs(new_qty), np.sign(new_qty)

        if not self.side:
            self.avg_price = 0

        # self._initialize_if_invalidqty(market_info)

        return realized_pnl


class Account:
    def __init__(self, time, wallet_balance):

        self.wallet_balance = wallet_balance

        self.margin_balance = wallet_balance
        self.margin_balance_low = wallet_balance

        self.margin_balances = [{'time':time,'margin_balance':wallet_balance}]

        qty, avg_price, unrealized_pnl = 0, 0, 0
        # self.position = Position(const.Position.NONE, qty, avg_price, unrealized_pnl)
        self.positions = {}

    def deposit(self, amount):
        self.balance += amount

    # def __dict__(self):
    #     return {
    #         'balance':self.balance,
    #     }

class Broker:

    def __init__(self, account : Account, data_list, strategy, fee):
        self.account = account

        self.data_list = data_list
        self.logger = Logger()
        # self.order_queues = []
        self.order_queues = {}

        self.strategy = strategy

        self.fee = fee

    def get_trades(self):
        df_trades = pd.DataFrame([o.__dict__() for o in self.logger.trade_infos])
        df_trades['pct'] = df_trades['pct_realized_pnl_by_price'] #
        df_trades['pct_margin'] = pct(df_trades['margin_balance'].shift(), df_trades['margin_balance'])
        df_trades['pct_wallet'] = pct(df_trades['margin_balance'].shift(), df_trades['margin_balance'])

        return df_trades

    def place_order(self, orders):
        for order in orders:
            order_id = util.generate_random_string()
            self.order_queues[order_id] = order

        # self.order_queues.extend(order)

    def execute_unfilled_orders(self, unpnl):

        trades = []

        #todo order sort check
        for order_id, order in list(self.order_queues.items()):
            symbol = order.symbol


            ##UPDATE FEE##
            amount = order.qty * order.price
            amount_fee = amount * self.fee
            self.account.wallet_balance -= amount_fee


            ##GET POSITION OF ORDERED SYMBOL##
            position = self.account.positions[symbol]

            #IF OPENING POSITION, set wallet_balance_at_open to calculate pnl pct at the close
            if(position.qty == 0):
                #todo check multi symbols logic
                #multi symbols close 다 한다음 해야함?
                #pct :  balance(using wallet_at_open) vs avg_price, final_price

                #todo
                #todo
                #margin balance 기준으로 qty long 들어갔으면 pct 계산 margin_at_open 으로 하는게 좋음!
                #strategy 별로 다름. strategy에서 계산해야할수도
                position.wallet_balance_at_open = self.account.wallet_balance


            #unpnl to update margin if closing position
            # unpnl = position.get_unrealized_pnl(order.price)

            #debug
            avg_price = position.avg_price
            open_qty = position.qty
            pct_realized_pnl_by_price = 0
            if(avg_price):
                pct_realized_pnl_by_price = pct(avg_price, order.price)


            ##UPDATE REALIZED_PNL TO WALLET_BALANCE: 0 IF OPENING ##
            realized_pnl = position.update(order.price, order.qty, order.side)
            self.account.wallet_balance += realized_pnl


            # IMPORTANT
            # trade.pct_realized_pnl should not be cummulated.
            # it is trade of each symbol, not multi symbols mdd, pctcum considered
            # pctcum = pct(INIT_BALANCE, FINAL_BALANCE)
            # mdd = mdd(margine_balaces)

            #get pct realized
            #todo check multi symbols logic
            #todo check multi symbols logic
            wallet_balance_at_open = position.wallet_balance_at_open
            pct_realized_pnl = pct(wallet_balance_at_open, wallet_balance_at_open + realized_pnl - amount_fee)

            # pct_realized_pnl = pct(self.account.wallet_balance, self.account.wallet_balance + realized_pnl - amount_fee)


            ##UPDATE MARGIN BALANCE, IF POSITION CLOSED##
            if (realized_pnl):
                # todo check

                #because of other symbols updating margin balance, can not set margin to wallet

                # undo unpnl
                self.account.margin_balance -= unpnl

                # add realized_pnl
                self.account.margin_balance += realized_pnl

                position.wallet_balance_at_open = None
                # todo  fee is unrelized pnl?

            ##TRADE RESULT##
            trade = Trade(order.symbol, order.time,
                          order.price, order.qty, order.side,

                          #todo qty
                          avg_price, open_qty,

                          realized_pnl,
                          pct_realized_pnl, pct_realized_pnl_by_price,
                          self.account.wallet_balance, self.account.margin_balance,

                          wallet_balance_at_open,
                          )

            # append
            trades.append(trade)

            #delete executed order
            del self.order_queues[order_id]

        return trades

    def get_result_stats(self):
        result = {}

        ##RESULTS##
        #
        # result['pctcum'] = pctcum
        # result['final_balance'] = final_balance

        #todo
        if (len(self.logger.trade_infos) == 0): return None, None, None

        df_trades = pd.DataFrame([o.__dict__() for o in self.logger.trade_infos])

        ##RESULT BY TRADES##
        # pct
        df_trades['pct'] = df_trades['pct_realized_pnl']
        df_trades['pct1'] = df_trades['pct'] + 1
        df_trades['pctcum'] = df_trades['pct1'].cumprod()
        # result['pctcum'] = df_trades['pctcum'].iloc[-1]

        ##qs##
        df_pcts = df_trades.set_index('time')[['pct']]
        df_pcts = pd.Series(df_pcts['pct'], index=df_pcts.index)
        result['sr'] = qs.stats.sharpe(df_pcts)
        result['win_rate'] = qs.stats.win_rate(df_pcts)
        result['num_trades'] = len(df_trades)
        # result['max_drawdown_trades'] = qs.stats.max_drawdown(df_pcts)

        ##RESULT BY BALANCES##
        #df margin_balances
        df_balances = pd.DataFrame(self.account.margin_balances)
        df_balances['pct'] = pct(df_balances['margin_balance'].shift(), df_balances['margin_balance'])
        df_balances['pctcum'] = pct(df_balances['margin_balance'].iloc[0], df_balances['margin_balance'])

        #pctcu
        #todo exclude last holding
        pctcum = pct(df_balances['margin_balance'].iloc[0], self.account.wallet_balance)
        result['pctcum'] = pctcum + 1

        #mdd
        df_pcts = pd.Series(df_balances['pct'], index=df_balances.index)
        result['max_drawdown'] = qs.stats.max_drawdown(df_pcts)
        return result, df_balances, df_trades

    def run(self):
        # iterator = range(len(self.data_list) - 1)
        # iter = tqdm(self.data_list)
        iter = self.data_list

        for data_symbols in iter: #at a time
            unpnls = 0
            wallet_balance_at_open = None
            for data_symbol in data_symbols:

                ##SET PRICE BY WHETHER LOOP IS AT OPEN or CLOSE##
                price = None
                if(data_symbol['is_open_bar']):
                    wallet_balance_at_open = self.account.wallet_balance
                    price = data_symbol['open']
                else:
                    price = data_symbol['close']

                ##UPDATE_UNREALIZED_PNL##
                symbol = data_symbol['symbol']

                #get position of current data symbol
                positions = self.account.positions

                #create position of symbol if the position is none value
                if(symbol not in positions):
                    qty, avg_price, unrealized_pnl = 0, 0, 0
                    positions[symbol] = Position(symbol, const.Position.NONE, qty, avg_price, unrealized_pnl)

                #update unpnl
                position = positions[symbol]
                #todo
                #todo bar 2번 가려면 data_symbol['open']['close'] 나눠야함. repeat tag로
                unpnl = position.get_unrealized_pnl(price)
                position.unrealized_pnl = unpnl

                #todo
                #todo
                #check values and logic
                #add all unpnl of symbols at a time, to addup to wallet_balance at the end
                unpnls += unpnl
                self.account.margin_balance = self.account.wallet_balance + unpnl


                ##GET_ORDERS##
                # state = {'account': self.account, 'data': data_symbol, 'unfilled_orders': self.order_queues}
                state = {'account': self.account, 'data': data_symbol}
                orders = self.strategy.execute(state)


                ##EXECUTE_ORDERS##
                self.place_order(orders)
                trades = self.execute_unfilled_orders(unpnl)

                #DEBUG
                #DEBUG
                #tod
                #todo debug small amount one trade : fee 떄문인듯
                result, df_balances, df_trades = self.get_result_stats()

                ##LOGGER##
                self.logger.trade_infos.extend(trades)
                self.logger.account_infos.append(self.account)

                ##MDD##
                #todo check
                self.account.margin_balances.append({
                                                    'time':data_symbol['time'],
                                                    'margin_balance':self.account.margin_balance,
                                                    'is_open_bar':data_symbol['is_open_bar']
                                                    } )


                if(data_symbol['is_open_bar']):
                    margin_balance_low = wallet_balance_at_open + unpnls_low

                    ##CHECK LIQUIDATED BY : LOW PNL##
                    #todo check current order low liquidation by testing data
                    position = positions[symbol]
                    #check bar 2번 하려면 low 한번만 해야함? 아닌듯
                    unpnl_low = position.get_unrealized_pnl(data_symbol['low'])
                    self.account.margin_balance_low = self.account.wallet_balance + unpnl_low
                    # self.account.margin_balance_low += unpnl_low

                    if(self.account.margin_balance_low <= 0):
                        #todo low add trades info
                        # print('liquidated at :\n{}\n'.format(data_symbol))
                        self.account.margin_balance = self.account.wallet_balance + unpnl_low
                        self.account.wallet_balance = self.account.wallet_balance + unpnl_low

                        self.account.margin_balances.append({
                            'time': data_symbol['time'],
                            'margin_balance': self.account.margin_balance,
                            'is_open_bar': data_symbol['is_open_bar']
                        })
                        return

##OPTIMIZER##
class Optimizer:

    def __init__(self):
        self.results = []
        pass

    ####PARAMS####
    def shuffle_params(self, params):
        for k in params:
            random.shuffle(params[k])

    def get_cartesian_params(self, params):
        params = [dict(zip(params, v)) for v in product(*params.values())]
        return params

    def optimize(self, data_list, strategy, param_batch, fee, strategy_name):

        #data dir
        now_str = datetime.datetime.now().strftime('%Y_%m_%d__%H_%M_%S')
        dir = '{}/data/optimizer_results/{}'.format(const.ROOT_DIR, now_str)
        os.makedirs(dir)

        #copy source files
        src_dir = '{}/bt_lite'.format(const.ROOT_DIR)
        tgt_dir = '{}/src/bt_lite'.format(dir)
        shutil.copytree(src_dir, tgt_dir)

        #copy source test files
        src_dir = '{}/test'.format(const.ROOT_DIR)
        tgt_dir = '{}/src/test'.format(dir)
        shutil.copytree(src_dir, tgt_dir)

        #save params and data
        pd.DataFrame(param_batch).to_csv('{}/param_batch.csv'.format(dir), index=False)
        pd.DataFrame(data_list).to_csv('{}/data_list.csv'.format(dir), index=False)

        #save results
        results_path = '{}/results.csv'.format(dir)
        print('results_path:\n{}\n'.format(results_path))
        results = []

        #todo dynamic column
        cols = ['pctcum', 'sr', 'max_drawdown', 'num_trades','win_rate']
        cols.extend(list(param_batch[0]))
        pd.DataFrame(columns=cols).to_csv(results_path, index=False)

        INITIAL_BALANCE = 1e3
        initial_time = data_list[0][0]['time']

        for i, param in enumerate(tqdm(param_batch)):
            if(i % 50 == 0):
                df = pd.DataFrame(results)
                if(len(df) > 0):
                    print(df.sort_values('pctcum')[-3:][['pctcum','sr','num_trades','max_drawdown', 'win_rate']])

                    # results to_csv
                    #single data, binance 값 확인 (open, close 정리)
                    pd.DataFrame(results).to_csv(results_path, mode='a', index=False, header=False)

            strategy.config = param

            ##ACCOUNT##
            account = Account(initial_time, INITIAL_BALANCE)

            ##BROKER RUN##
            broker = Broker(account, data_list, strategy, fee)
            broker.run()


            #liquidiated
            #todo include to result?
            if(broker.account.wallet_balance < 0): continue



            ##RESULT METRICS##
            #get result
            result, _, _ = broker.get_result_stats()
            if(result == None): continue

            #DEBUG
            #get_trades
            df_trades = broker.get_trades()
            s = io.StringIO()
            df_trades.to_csv(s, index=False)
            result['trades'] = s.getvalue()
            print('result[trades]:\n{}\n'.format(result['trades']))


            #update result
            result.update(param)

            #results append
            results.append(result)
            # break

        #results to_csv
        df_results = pd.DataFrame(results)

        #save only the best 1000
        df_results = df_results.sort_values('pctcum')[-1000:]
        df_results.to_csv(results_path, index=False)

        #rename dir
        pctcum_max = df_results['pctcum'].max()
        dir_new = dir + '_{}_{}'.format(strategy_name, int(pctcum_max))
        os.rename(dir, dir_new)
        print('results dir:\n{}\n'.format(dir_new))

        return results

