import datetime

import numpy as np
import pandas as pd

from bt_lite.common import const, util
from bt_lite.common.util import pct, divide_chunks
from bt_lite.core import Order, Broker, Account, Optimizer
from bt_lite.indicators import moving_average
from bt_lite.strategy import Strategy
from test.test_util import read, get_tops_by_vol


class DrawDownStrgy(Strategy):

    def __init__(self):
        super().__init__()
        self.config = None

        self.df_cds1d = None

        self.num_long = 0
        self.traded_today = False

    def get_indicators(self, data, indicators):
        ##1d##
        #shift
        self.df_cds1d['high_s1'] = self.df_cds1d.groupby('symbol')['high'].shift()
        self.df_cds1d['low_s1'] = self.df_cds1d.groupby('symbol')['low'].shift()
        self.df_cds1d['close_s1'] = self.df_cds1d.groupby('symbol')['close'].shift()
        


        #emas
        emas = indicators['emas']
        sfts = [1]
        moving_average.set_ema(self.df_cds1d, emas, sfts)
        moving_average.set_pct_ema_close(self.df_cds1d, emas, sfts)

        ##1h##
        df_cds = data

        #shift ohlc
        cols_shift = ['open', 'high', 'low', 'close']
        for col_shift in cols_shift:
            util.shift(df_cds, col_shift, 1)

        #drawdown
        # self.df_cds['cummax'] = self.df_cds['close'].cummax()
        df_cds['cummax'] = df_cds['open'].rolling(90).max()
        df_cds['drawdown'] = df_cds['open'] / df_cds['cummax'] - 1

        return df_cds

    def execute(self, state):
        #orders
        orders = []

        #1d
        # df_cds1d = self.df_cds1d

        #account
        account = state['account']

        #data
        data = state['data']
        time = data['time']
        symbol = data['symbol']

        #balance
        wallet_balance = account.wallet_balance
        margin_balance_low = account.margin_balance_low
        if(wallet_balance < 0): return orders
        if(margin_balance_low < 0):
            # print('wallet_balance_low < 0')
            return orders


        #position
        position = account.positions[symbol]
        position_side = position.side
        position_qty = position.qty
        pct_unpnl = position.unrealized_pnl

        #leverage
        lev = self.config['lev']

        #num_splits
        num_splits = self.config['num_splits']

        #max hold symbols
        max_hold_symbols = self.config['max_hold_symbols']


        if (position_side == const.Position.NONE):
            # LONG
            if (
                    # (not self.traded_today) &
                    (self.config['dd_long'] < data['drawdown'])
            ):
                # todo open or close shift

                # tgt long qty
                qty = wallet_balance * lev / num_splits / max_hold_symbols / data['open']

                order = Order(time=time, symbol=symbol, side=const.OrderType.LONG, qty=qty, price=data['open'])
                orders.append(order)
                self.num_long += 1
                self.traded_today = True

        elif (position_side == const.Position.LONG):

            #TIME CUT
            if (time.hour == 9):
                order = Order(time=time, symbol=symbol, side=const.OrderType.SHORT, qty=position_qty, price=data['open'])

                orders.append(order)
                self.traded_today = False

            #TAKE PROFIT
            elif (pct_unpnl > self.config['pct_tp']):
                order = Order(time=time, symbol=symbol, side=const.OrderType.SHORT, qty=position_qty, price=data['open'])

                orders.append(order)
                self.num_long = 0
                self.traded_today = False

            #STOP LOSS
            # elif (pct_unpnl < self.config['pct_sl']):
            #     order = Order(time=time, symbol=symbol, side=const.OrderType.SHORT, qty=position_qty, price=data['open'])
            #     # print('short:\n{}\n'.format(data))
            #     orders.append(order)
            #     self.num_long = 0
            #     self.traded_today = False

            #SPLIT LONG
            elif (
                    (self.num_long  == 1) &
                    (pct_unpnl < self.config['long1_pnl_pct'])
            ):
                # tgt long qty
                qty = wallet_balance * lev / num_splits / max_hold_symbols / data['open']

                order = Order(time=time, symbol=symbol, side=const.OrderType.LONG, qty=qty, price=data['open'])
                orders.append(order)
                self.num_long += 1
            #
            # # SPLIT LONG
            elif (
                    (self.num_long  == 2) &
                    (pct_unpnl < self.config['long2_pnl_pct'])
            ):
                # tgt long qty
                qty = wallet_balance * lev / num_splits / max_hold_symbols / data['open']

                order = Order(time=time, symbol=symbol, side=const.OrderType.LONG, qty=qty, price=data['open'])
                orders.append(order)
                self.num_long += 1

        return orders

def run_single(df_cds, df_cds1d, config, fee):
    df_cds_sub = df_cds.copy()

    ##STRATEGY##
    strategy = DrawDownStrgy()
    strategy.df_cds1d = df_cds1d

    # get_indicators
    indicators = {
        'emas':[7,14,25,99]
    }
    df_cds_sub = strategy.get_indicators(df_cds_sub, indicators)
    # df_cds_sub = df_cds_sub[df_cds_sub['time'] > '2021-05-16']
    # data_list = util.df2list(df_cds_sub)
    data_list = util.df2list_by_date_group(df_cds_sub)

    # todo 청산 ㅠ, timecut?
    # param 조절
    # opt 할때 청산 되면 빼기?

    strategy.config = config

    ##ACCOUNT##
    INITIAL_BALANCE = 1e3
    initial_time = df_cds_sub.iloc[0]['time']
account = Account(initial_time, INITIAL_BALANCE)
##RUN BROKER##
    broker = Broker(account, data_list, strategy, fee)
    broker.run()
    final_balance = broker.account.wallet_balance
    print('final_balance:\n{}\n'.format(final_balance))


    pctcum = pct(INITIAL_BALANCE, final_balance)
    print('final_balance pctcum :\n{}\n'.format(pctcum))

    return broker



def run_opt(df_cds, df_cds1d, fee, strategy_name, nlargest, max_hold_symbols):
    ##
    df_cds_sub = df_cds.copy()
    print('len(df_cds_sub):\n{}\n'.format(len(df_cds_sub)))

    ##STRATEGY##
    strategy = DrawDownStrgy()
    strategy.df_cds1d = df_cds1d

    # get_indicators
    indicators = { 'emas': [2,3,5,7,14,21,25,99], }
    df_cds_sub = strategy.get_indicators(df_cds_sub, indicators)

    #2list
    # data_list = util.df2list(df_cds_sub)
    # data_list = list(divide_chunks(data_list, nlargest))
    data_list = util.df2list_by_date_group(df_cds_sub)

    ##OPTIMIZER##
    optimizer = Optimizer()

    #params
    params = {}

    #large range
    params['lev'] = [5]
    params['dd_long'] = np.arange(-50/100,-10/100,5/100).tolist()
    params['num_splits'] = [3]
    params['max_hold_symbols'] = [max_hold_symbols]
    params['long1_pnl_pct'] = np.arange(-100/100,-30/100,5/100).tolist()
    params['long2_pnl_pct'] = np.arange(-100/100,-30/100,5/100).tolist()
    params['pct_tp'] = np.arange(5/100,50/100,5/100).tolist()

    #small range
    # params['lev'] = [5]
    # params['dd_long'] = np.arange(-50/100,-10/100,20/100).tolist()
    # params['num_splits'] = [3]
    # params['long1_pnl_pct'] = np.arange(-100/100,-30/100,20/100).tolist()
    # params['long2_pnl_pct'] = np.arange(-100/100,-30/100,20/100).tolist()
    # params['pct_tp'] = np.arange(5/100,50/100,20/100).tolist()

    optimizer.shuffle_params(params)
    param_batch = optimizer.get_cartesian_params(params)

    #run
    results = optimizer.optimize(data_list, strategy, param_batch, fee, strategy_name)
    df = pd.DataFrame(results)
    print(df.sort_values('pctcum')[-10:][['pctcum','sr','max_drawdown','num_trades']])


#FEE
LIMIT_FEE = 0.036 / 100
fee = LIMIT_FEE

# FRAME
exchange = "binance_spot"

#symbol
# symbols = ['ETH/USDT']
symbols = ['ETH/USDT', 'BTC/USDT']

##READ##
df_cds1d = read(exchange, '1d', symbols)
df_cds = read(exchange, '1h', symbols)
df_cds['time'] = df_cds['time'] + datetime.timedelta(hours=9)
print('df_cds range:{}~{}'.format(df_cds.iloc[0]['time'], df_cds.iloc[-1]['time']))
df_cds = df_cds[df_cds['time'] >= '2020-03-15 09:00:00']
df_cds = df_cds.reset_index(drop=True)

# tops by volume
nlargest = len(symbols)
max_hold_symbols = nlargest
ma = 5
df_cds_tops = get_tops_by_vol(df_cds, ma, nlargest)

##RUN SINGLE##
config = {
    'lev': 4,
    'max_hold_symbols': max_hold_symbols,
    'dd_long': -40 / 100,

    'num_splits': 3,
    'long1_pnl_pct': -10 / 100,
    'long2_pnl_pct': -20 / 100,
    'pct_tp': 40 / 100
}

broker = run_single(df_cds, df_cds1d, config, fee)
result, _, _ = broker.get_result_stats()
print('run_single result:\n{}\n'.format(result))

#RUN OPT
strategy_name = 'drawdown'
run_opt(df_cds, df_cds1d, fee, strategy_name, nlargest, max_hold_symbols)

