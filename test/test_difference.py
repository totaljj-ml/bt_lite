import pandas as pd

from bt_lite.common import const
from test.test_util import read, get_tops_by_vol


def get_difference(df1, df2):

    return pd.concat([df1, df2]).drop_duplicates(keep=False,subset=['symbol','time'])

if __name__ == '__main__':
    # #vola_strategy.py
    # df1 = pd.read_csv('{}/data/test/test.csv'.format(const.ROOT_DIR))[['time']]
    # # df1 = df1[df1['side'] == -1]
    # # df1['time'] = df1['time'].str[:10]
    # print(len(df1))
    # print(df1[:1])
    #
    # #ma_simple.py
    # df2 = pd.read_csv('{}/data/test/test_tops.csv'.format(const.ROOT_DIR))[['time']]
    # print(len(df2))
    # print(df2[:1])
    #
    # df_diff = get_difference(df1, df2)
    # print(df_diff)
    #
    # FEE
    LIMIT_FEE = 0.036 / 100
    fee = LIMIT_FEE

    # FRAMES
    frame = '1d'
    symbols = ['ETH/USDT', 'BTC/USDT']

    exchange = 'binance_spot'
    exchange = 'binance_future'

    ##READ##
    df_cds = read(exchange, frame, symbols)
    df_cds = df_cds[df_cds['time'] > '2021-01-01']
    df_cds = df_cds.reset_index(drop=True)
    print('df_cds range:{}~{}'.format(df_cds.iloc[0]['time'], df_cds.iloc[-1]['time']))

    # tops by volume
    nlargest = 1
    ma = 2
    df_cds_tops = get_tops_by_vol(df_cds, ma, nlargest)
    print('df_cds_tops range:{}~{}'.format(df_cds_tops.iloc[0]['time'], df_cds_tops.iloc[-1]['time']))

    df_diff = get_difference(df_cds, df_cds_tops)
    print(df_diff[['time','symbol']])
    # print(df_diff[df_diff['symbol'] == 'BTC/USDT'])
    # print(df_diff[df_diff['symbol'] == 'ETH/USDT'])




