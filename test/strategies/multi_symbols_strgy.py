import datetime

import numpy as np
import pandas as pd

from bt_lite.common import const, util
from bt_lite.common.util import pct, divide_chunks
from bt_lite.core import Order, Broker, Account, Optimizer
from bt_lite.indicators import moving_average, tops
from bt_lite.strategy import Strategy
from test.test_util import read



#leverage by last trade, mdd?
class MultiSymsStrgy(Strategy):

    def __init__(self):
        super().__init__()
        self.config = None

        self.num_long = 0
        self.tgt_price = None

        self.traded_today = False
        self.df_today = None

    def get_indicators(self, data, config ):
        df_cds = data
        df_cds['high_s1'] = df_cds.groupby('symbol')['high'].shift()
        df_cds['low_s1'] = df_cds.groupby('symbol')['low'].shift()
        df_cds['close_s1'] = df_cds.groupby('symbol')['close'].shift()

        emas = config['emas']
        sfts = [1]
        moving_average.set_ema(df_cds, emas, sfts)


        return df_cds

    def execute(self, state):
        #orders
        orders = []

        #account
        account = state['account']

        #data
        data = state['data']
        time = data['time']
        symbol = data['symbol']

        #balance
        wallet_balance = account.wallet_balance
        margin_balance_low = account.margin_balance_low
        if(wallet_balance < 0): return orders
        if(margin_balance_low < 0):
            # print('wallet_balance_low < 0')
            return orders

        


        position = account.positions[symbol]

        #position
        position_side = position.side
        position_qty = position.qty

        #leverage
        lev = self.config['lev']

        #num_splits
        num_splits = self.config['num_splits']


        if (position_side == const.Position.NONE):
            # LONG
            if (
                    # (self.tgt_price > data['low'])
                    (data['open'] > data['ema99_s1'])
            ):
                # todo open or close shift

                # tgt long qty
                open_price = data['open']
                # open_price = self.tgt_price
                qty = wallet_balance * lev / num_splits / open_price

                order = Order(time=time, symbol=symbol, side=const.OrderType.LONG, qty=qty, price=open_price)
                orders.append(order)
                self.num_long += 1
                self.traded_today = True
                print('long:\n{}\n'.format(data))

        elif (position_side == const.Position.LONG):

            #TIME CUT
            if (time.hour == 9):
                order = Order(time=time, symbol=symbol, side=const.OrderType.SHORT, qty=position_qty, price=data['open'])

                orders.append(order)
                self.traded_today = False

            #TAKE PROFIT
            elif (pct_unpnl > self.config['pct_tp']):
                order = Order(time=time, symbol=symbol, side=const.OrderType.SHORT, qty=position_qty, price=data['open'])

                orders.append(order)
                self.num_long = 0
                self.traded_today = False

            #STOP LOSS
            # elif (pct_unpnl < self.config['pct_sl']):
            #     order = Order(time=time, symbol=symbol, side=const.OrderType.SHORT, qty=position_qty, price=data['open'])
            #     # print('short:\n{}\n'.format(data))
            #     orders.append(order)
            #     self.num_long = 0
            #     self.traded_today = False

            #SPLIT LONG
            # elif (
            #         (self.num_long  == 1) &
            #         (pct_unpnl < self.config['long1_pnl_pct'])
            # ):
            #     # tgt long qty
            #     qty = wallet_balance * lev / num_splits / max_hold_symbols / data['open']
            #
            #     order = Order(time=time, symbol=symbol, side=const.OrderType.LONG, qty=qty, price=data['open'])
            #     orders.append(order)
            #     self.num_long += 1
            #
            # # SPLIT LONG
            # elif (
            #         (self.num_long  == 2) &
            #         (pct_unpnl < self.config['long2_pnl_pct'])
            # ):
            #     # tgt long qty
            #     qty = wallet_balance * lev / num_splits / max_hold_symbols / data['open']
            #
            #     order = Order(time=time, symbol=symbol, side=const.OrderType.LONG, qty=qty, price=data['open'])
            #     orders.append(order)
            #     self.num_long += 1

        return orders

def run_single(df_cds, nlargest, fee):
    df_cds_sub = df_cds.copy()

    ##STRATEGY##
    strategy = MultiSymsStrgy()

    # get_indicators
    indicators = {
        'emas':[7,14,25,99]
    }
    df_cds_sub = strategy.get_indicators(df_cds_sub, indicators)
    # df_cds_sub = df_cds_sub[df_cds_sub['time'] > '2021-05-16']

    data_list = util.df2list_by_date_group(df_cds_sub)

    config = {
        'lev' : 4, 'k': 0.4,
        'num_splits':1,
        # 'long1_pnl_pct':-10/100, 'long2_pnl_pct':-20/100,
        'pct_tp': 40 / 100
    }
    strategy.config = config

    ##ACCOUNT##
    INITIAL_BALANCE = 1e3
    initial_time = df_cds_sub.iloc[0]['time']
account = Account(initial_time, INITIAL_BALANCE)
##RUN BROKER##
    broker = Broker(account, data_list, strategy, fee)
    broker.run()
    final_balance = broker.account.wallet_balance
    print('final_balance:\n{}\n'.format(final_balance))

    pctcum = pct(INITIAL_BALANCE, final_balance)
    pctcum = pct(INITIAL_BALANCE, final_balance)
    print('final_balance pctcum :\n{}\n'.format(pctcum))

    return broker

def read1d():
    from bt_lite.data import data_reader


    reader = data_reader.SqlReader()

    # 1d
    df_cds = reader.read_ohlcv('binance_future', '1d')
    # df_cds = df_cds[df_cds['symbol'] == 'ETH/USDT']
    # df_cds1d = test_util.resample_cds(df_cds, '1d')
    return df_cds

def run_opt(df_cds, df_cds1d, fee, strategy_name, nlargest):
    ##
    df_cds_sub = df_cds.copy()
    print('len(df_cds_sub):\n{}\n'.format(len(df_cds_sub)))

    ##STRATEGY##
    strategy = MultiSymsStrgy()
    strategy.df_cds1d = df_cds1d

    # get_indicators
    indicators = { 'emas': [2,3,5,7,14,21,25,99], }
    df_cds_sub = strategy.get_indicators(df_cds_sub, indicators)

    #2list
    data_list = util.df2list_by_date_group(df_cds_sub)

    ##OPTIMIZER##
    optimizer = Optimizer()

    #params
    params = {}

    #large range
    params['lev'] = [2,3,4]
    params['k'] = [0.4,0.6]
    params['num_splits'] = [1]
    # params['long1_pnl_pct'] = []
    # params['long2_pnl_pct'] =
    params['pct_tp'] = np.arange(-0.02, 0, 0.01).tolist()

    optimizer.shuffle_params(params)
    param_batch = optimizer.get_cartesian_params(params)

    #run
    results = optimizer.optimize(data_list, strategy, param_batch, fee, strategy_name)
    df = pd.DataFrame(results)
    print(df.sort_values('pctcum')[-10:][['pctcum','sr','max_drawdown','num_trades']])


#FEE
LIMIT_FEE = 0.036 / 100
fee = LIMIT_FEE

frame = '1d'

##READ##
df_cds = read1d()
# df_cds['time'] = df_cds['time'] + datetime.timedelta(hours=9)
df_cds = df_cds[df_cds['symbol'].isin(['ETH/USDT','BTC/USDT'])]
df_cds = df_cds.reset_index(drop=True)
print('df_cds range:{}~{}'.format(df_cds.iloc[0]['time'], df_cds.iloc[-1]['time']))
# df_cds = df_cds[df_cds['time'] >= '2020-03-15 09:00:00']


#
nlargest = 20
mas = [3,5]
for ma in mas:
    moving_average.set_volume_ma(df_cds, ma)
    df_tops = tops.get_nlargest(df_cds, 'vol_ma{}_s1'.format(ma), nlargest)


# ##RUN SINGLE##
broker = run_single(df_tops, nlargest, fee)
result, _, _ = broker.get_result_stats()
print('run_single result:\n{}\n'.format(result))
#
# #RUN OPT
# strategy_name = 'drawdown'
# run_opt(df_cds, df_cds1d, fee, strategy_name, nlargest)


#todo
#df_cds 일별로 nlargest 나누기