import os

ROOT_DIR = os.path.dirname(os.path.abspath(__file__)) + '/../../'
CONFIG_FILE = ROOT_DIR + 'conf.yaml'

class DB:
    HOST = 'localhost'
    PORT = 3306

    ID = 'bt_lite'
    PWD = 'bt_lite1'
    NAME = 'bt_lite'


BINANCE_SPOT_FEE = 0.036/100
BINANCE_FUTURE_FEE = 0.036/100

class Position:
    LONG = 1
    NONE = 0
    SHORT = -1

class OrderType:
    LONG = 1
    SHORT = -1

